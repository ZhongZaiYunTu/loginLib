package com.zz.yt.test;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.blankj.utilcode.util.ActivityUtils;
import com.zz.yt.lib.login.delegate.login.LoginDelegate;
import com.zz.yt.lib.login.delegate.login.LoginFaceDelegate;
import com.zz.yt.lib.login.delegate.login.LoginTabDelegate;
import com.zz.yt.lib.login.delegate.login.LoginTravelDelegate;
import com.zz.yt.lib.login.delegate.login.LoginTwoDelegate;
import com.zz.yt.lib.login.delegate.login.SignInCloudDelegate;
import com.zz.yt.lib.login.delegate.login.SignInCodeDelegate;
import com.zz.yt.lib.login.delegate.login.SignInDelegate;
import com.zz.yt.lib.login.delegate.login.SignInFaceDelegate;
import com.zz.yt.lib.login.delegate.login.SignInFingerDelegate;
import com.zz.yt.lib.login.delegate.login.SignInTravelDelegate;
import com.zz.yt.lib.login.utils.LoginUtils;
import com.zz.yt.lib.ui.CustomTitleBar;
import com.zz.yt.lib.ui.base.ui.LatteTitleDelegate;
import com.zz.yt.lib.ui.delegate.edition.record.EditionRecordDelegate;

/**
 * 选择登录界面
 *
 * @author qf
 * @version 1.0
 */
public class TestSignDelegate extends LatteTitleDelegate {

    @Override
    protected void setCustomTitleBar(@NonNull CustomTitleBar titleBar) {
        titleBar.setText("选择登录界面");
    }

    @Override
    protected Object setLayout() {
        return R.layout.activity_task;
    }

    @Override
    public void onBindView(@Nullable Bundle savedInstanceState, @NonNull View rootView) {
        super.onBindView(savedInstanceState, rootView);
        getButton(R.id.id_btn_zw).setOnClickListener(v -> LoginUtils.setFinger(null));
        getButton(R.id.id_btn_travel).setOnClickListener(v -> start(new LoginTravelDelegate()));
        getButton(R.id.id_btn_travel2).setOnClickListener(v -> start(new SignInTravelDelegate()));
        getButton(R.id.id_btn_aa).setOnClickListener(v -> start(new LoginTabDelegate()));
        getButton(R.id.id_btn_bb).setOnClickListener(v -> start(new SignInCloudDelegate()));
        getButton(R.id.id_btn_0).setOnClickListener(v -> start(EditionRecordDelegate.create()));
        getButton(R.id.id_btn_1).setOnClickListener(v -> start(new LoginDelegate()));
        getButton(R.id.id_btn_2).setOnClickListener(v -> start(new LoginFaceDelegate()));
        getButton(R.id.id_btn_3).setOnClickListener(v -> start(new LoginTwoDelegate()));
        getButton(R.id.id_btn_4).setOnClickListener(v -> start(new SignInCodeDelegate()));
        getButton(R.id.id_btn_5).setOnClickListener(v -> start(new SignInDelegate()));
        getButton(R.id.id_btn_6).setOnClickListener(v -> start(new SignInFaceDelegate()));
        getButton(R.id.id_btn_7).setOnClickListener(v -> start(new SignInFingerDelegate()));
        getButton(R.id.id_btn_prompt).setOnClickListener(v -> start(PromptDelegate.create()));


    }
}
