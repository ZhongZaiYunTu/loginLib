package com.zz.yt.test;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.whf.android.jar.net.gson.HttpGsonUtils;
import com.zz.yt.lib.login.grade.UpDateUtils;
import com.zz.yt.lib.login.grade.UpGradeUtils;
import com.zz.yt.lib.login.grade.UpVersionUtils;
import com.zz.yt.lib.login.grade.bean.GradeBean;
import com.zz.yt.lib.ui.CustomTitleBar;
import com.zz.yt.lib.ui.base.ui.LatteTitleDelegate;

/***
 * 系统集成弹框
 * @author qf
 * @version 1.0
 */
public class PromptDelegate extends LatteTitleDelegate implements AdapterView.OnItemClickListener {

    private GradeBean mGradeBean = null;

    private final String[] titles = {
            "更新1", "更新2", "更新3"
    };

    @NonNull
    public static PromptDelegate create() {
        final Bundle args = new Bundle();

        final PromptDelegate fragment = new PromptDelegate();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if ("更新1".equals(titles[position])) {
            UpDateUtils.create()
                    .setDate(mGradeBean)
                    .show();
        } else if ("更新2".equals(titles[position])) {
            UpGradeUtils.create()
                    .setDate(mGradeBean)
                    .show();
        } else if ("更新3".equals(titles[position])) {
            UpVersionUtils.create()
                    .setDate(mGradeBean)
                    .show();

        }
    }

    @Override
    protected void setCustomTitleBar(@NonNull CustomTitleBar titleBar) {
        titleBar.setText("更新弹框");
    }

    @Override
    public Object setLayout() {
        return R.layout.activity_prompt;
    }

    @Override
    public void onBindView(@Nullable Bundle savedInstanceState, @NonNull View rootView) {
        super.onBindView(savedInstanceState, rootView);
        ListView mListView = findViewById(R.id.id_list_view);
        if (mListView != null) {

            ArrayAdapter<String> adapter = new ArrayAdapter<>(
                    getProxyActivity(),
                    android.R.layout.simple_list_item_1,
                    titles);
            mListView.setAdapter(adapter);
            mListView.setOnItemClickListener(this);
        }
        String json = "{\"apkSize\":\"36.5MB\",\"appDesc\":\"供销快递\",\"appFile\":\"0d7b0fc1548b4557949d116e3ba1c813\",\"appFileName\":\"供销快递20231011_1701_release_3.apk\",\"appId\":\"ce4bb67802658565cde3ed923fcc34c1\",\"appName\":\"供销快递\",\"compelUpdate\":\"1\",\"compelUpdateCh\":\"开启\",\"content\":\"\\\\u003Cp style=\\\\\\\"box-sizing: inherit; color: #606266; font-family: 'Helvetica Neue', Helvetica, 'PingFang SC', 'Hiragino Sans GB', 'Microsoft YaHei', Arial, sans-serif; font-size: 14px; background-color: #ffffff;\\\\\\\"\\\\u003E【优化】优化批量下单收件人选择逻辑。\\\\u003C/p\\\\u003E\\n\\\\u003Cp style=\\\\\\\"box-sizing: inherit; color: #606266; font-family: 'Helvetica Neue', Helvetica, 'PingFang SC', 'Hiragino Sans GB', 'Microsoft YaHei', Arial, sans-serif; font-size: 14px; background-color: #ffffff;\\\\\\\"\\\\u003E【优化】修复若干已知问题，提高稳定性。\\\\u003C/p\\\\u003E\\n\\\\u003Cp style=\\\\\\\"box-sizing: inherit; color: #606266; font-family: 'Helvetica Neue', Helvetica, 'PingFang SC', 'Hiragino Sans GB', 'Microsoft YaHei', Arial, sans-serif; font-size: 14px; background-color: #ffffff;\\\\\\\"\\\\u003E【优化】优化产品体验。\\\\u003C/p\\\\u003E\",\"createBy\":\"admin\",\"createTime\":\"2023-10-11 17:26:26\",\"deleted\":0,\"downloadCount\":0,\"extendData\":\"{}\",\"icon\":\"71e91ab64b5443cc9434c47178829374\",\"id\":\"5ab16dcade200a623dff71a6417736ed\",\"osType\":\"0\",\"osTypeCh\":\"安卓\",\"pics\":\"903df028c80449308e51167cd37a6259\",\"updateTime\":\"2023-10-11 17:26:26\",\"version\":0,\"versionCode\":3,\"versionName\":\"v1.0.3\"}";
        mGradeBean = HttpGsonUtils.fromJson(json, GradeBean.class);
    }

}
