package com.zz.yt.test;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.os.Bundle;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.ColorUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.whf.android.jar.app.Latte;
import com.whf.android.jar.base.delegate.BaseDelegate;
import com.whf.android.jar.constants.UserConstant;
import com.whf.android.jar.util.RandomUntil;
import com.whf.android.jar.util.log.LatteLogger;
import com.whf.android.jar.util.storage.LattePreference;
import com.zz.yt.lib.login.BaseLoginActivity;
import com.zz.yt.lib.login.badge.BadgeTimingUtils;
import com.zz.yt.lib.login.constant.ILoginListener;
import com.zz.yt.lib.login.constant.ISignCodeListener;
import com.zz.yt.lib.login.constant.ISignUpListener;
import com.zz.yt.lib.login.delegate.debug.LoginDebugUtils;
import com.zz.yt.lib.login.launcher.privacy.ScrollLauncher;
import com.zz.yt.lib.login.utils.LoginUtils;
import com.zz.yt.lib.login.utils.SpannableStringUtils;

import java.util.ArrayList;

/**
 * 登录，启动，滑动的demo
 *
 * @author qf
 * @version 2020-04-16
 */
public class LoginActivity extends BaseLoginActivity implements ILoginListener, ISignCodeListener,
        ISignUpListener {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ScrollLauncher.setScrollLauncher(true);
        Latte.getConfigurator().withActivity(this);
        LattePreference.addCustomAppProfile(UserConstant.USER_PHONE, "15086921298");
    }

    @Override
    protected void appInit() {
        //文件服务 初始化
        App.init();
    }

    @Override
    protected void startMain() {
        ActivityUtils.startActivity(MainActivity.class);
        finish();
    }

    @Override
    public BaseDelegate outSignInDelegate() {
        return new TestSignDelegate();
    }

    @Override
    public Object setLauncher() {
        return R.mipmap.launcher_00;
    }

    @Override
    public ArrayList<Object> setLauncherScroll() {
        ArrayList<Object> arrayList = new ArrayList<>();
        arrayList.add("http://image.biaobaiju.com/uploads/20181007/16/1538902328-aSbPWXFQNh.jpg");
        arrayList.add(R.mipmap.launcher_01);
        arrayList.add(R.mipmap.launcher_02);
        arrayList.add(R.mipmap.launcher_03);
        arrayList.add(R.mipmap.launcher_04);
        return arrayList;
    }

    @Override
    public Object setSignIn() {
        return R.drawable.ic_login_bg;
    }

    @Override
    public void setServiceSport(@NonNull TextView textDebug) {
        textDebug.setVisibility(View.VISIBLE);
        LoginDebugUtils.create(textDebug, 8081);
//        LoginServerUtils.create(textDebug, "WXHXPAQSC");
    }

    @Override
    public void onSignIn(String phone, String code) {
        startMain();
    }

    @Override
    public void onPhoneCode(String phone) {
        ToastUtils.showShort("給" + phone + "发送验证码");
    }

    @Override
    public void onPhoneSignIn(String phone, String code) {
        startMain();
    }


    @Override
    public boolean isSignUp() {
        return true;
    }

    @Override
    public void setSignUpView(View rootView, TextView signUpView) {
        if (signUpView != null) {
            signUpView.setText(SpannableStringUtils.getBuilder("还没有账号？ ")
                    .append("立即注册")
                    .setClickSpan(clickableSpan)
                    .create());
            signUpView.setMovementMethod(LinkMovementMethod.getInstance());
        }
    }

    @Override
    public void setSignInView(Button signInView) {

    }


    private final ClickableSpan clickableSpan = new ClickableSpan() {
        @Override
        public void onClick(@NonNull View widget) {
            LatteLogger.i("事件触发了 立即注册");
            ToastUtils.showShort("事件触发了 立即注册");
        }

        @Override
        public void updateDrawState(@NonNull TextPaint ds) {
            ds.setColor(ColorUtils.getColor(R.color.red));
            ds.setUnderlineText(true);
        }
    };

    @Override
    protected void onUserLeaveHint() {
        super.onUserLeaveHint();
        //添加角标(刷新频率3秒)
        BadgeTimingUtils.create(3, index -> {
            //添加角标
            LoginUtils.setBadge(RandomUntil.getHum(100));
        });
    }

}
