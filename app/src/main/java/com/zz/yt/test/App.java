package com.zz.yt.test;

import android.app.Application;

import com.whf.android.jar.app.Latte;
import com.whf.android.jar.constants.HttpConstant;
import com.whf.android.jar.net.HttpCode;
import com.zz.yt.lib.annex.QbSdkUtils;
import com.zz.yt.lib.login.utils.LoginUtils;

/**
 * @author qf
 * @version 2020-04-16
 */
public class App extends Application {


    /*** 检查更新（中电云）*/
    String CHECK_VERSION = "http://124.126.103.76:8666/api/app/checkVersion";

    /*** 历史信息（中电云）*/
    String CHECK_HISTORY = "http://124.126.103.76:8666/api/app/getHistory" +
            "?sort=create_time&order=desc&appId=";

    @Override
    public void onCreate() {
        super.onCreate();

        //全局初始化
        Latte.init(this)
                .withGuide(3)
                .withGuide(false)
                .withLoaderDelayed(1000)
                .withAdaptation(360f)
                .withApiCheckVersion(CHECK_VERSION)
                .withApiCheckHistory(CHECK_HISTORY)
                .withApiHost("http://www.baidu.com")
                .withAppId("3b1dffaec121205d3b50b86170813159")
                .withApplicationId(BuildConfig.APPLICATION_ID)
                .withJavascriptInterface("http://www.baidu.com")
                .withFtpHost("http://www.baidu.com")
                .withWebHost("http://www.baidu.com")
                .withLoggable(BuildConfig.DEBUG)
                .withVersionCode(BuildConfig.VERSION_CODE)
                .withVersionName(BuildConfig.VERSION_NAME)
                .withSignExpiration(HttpCode.CODE_403)
                .withKeyMessage(HttpConstant.MESSAGE)
                .withAppIcon(R.mipmap.ic_launcher)
                .withSwitchingServices(true)
                .withHttpLogging()
                .withInterceptor()
                .configure();
    }

    public static void init() {

        //文件服务 初始化
        QbSdkUtils.init();

        //初始化角标设置
        LoginUtils.initBadge(LoginActivity.class);
    }
}
