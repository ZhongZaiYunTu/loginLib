package com.zz.yt.lib.login.delegate;

import android.content.Context;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.blankj.utilcode.util.ColorUtils;
import com.zz.yt.lib.login.R;
import com.zz.yt.lib.login.checker.AccountManager;
import com.zz.yt.lib.login.checker.IUserChecker;
import com.zz.yt.lib.login.launcher.ILauncherListener;
import com.zz.yt.lib.login.launcher.OnLauncherFinishTag;
import com.zz.yt.lib.login.launcher.privacy.PrivacyLauncher;
import com.zz.yt.lib.ui.CustomTitleBar;
import com.zz.yt.lib.ui.base.ui.LatteTitleDelegate;
import com.zz.yt.lib.ui.delegate.web.WebUiDelegate;
import com.zz.yt.lib.ui.privacy.PrivacyDao;


/**
 * 用户隐私保护
 *
 * @author qf
 * @version 1.0
 */
public class LauncherPrivacyDelegate extends LatteTitleDelegate implements View.OnClickListener {


    private ILauncherListener mLauncherListener = null;

    @NonNull
    public static LauncherPrivacyDelegate create() {
        Bundle args = new Bundle();

        final LauncherPrivacyDelegate fragment = new LauncherPrivacyDelegate();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof ILauncherListener) {
            mLauncherListener = (ILauncherListener) context;
        }
    }

    @Override
    public Object setLayout() {
        return R.layout.hai_delegate_launcher_privacy;
    }

    @Override
    protected void setCustomTitleBar(@NonNull CustomTitleBar titleBar) {
        String title = getResources().getString(R.string.privacy_title);
        titleBar.setText(title);
        titleBar.getTitleBarLeftBtn().setVisibility(View.INVISIBLE);
        titleBar.getTitleBarRightBtn().setVisibility(View.INVISIBLE);
    }

    @Override
    public void onLazyInitView(@Nullable Bundle savedInstanceState) {
        super.onLazyInitView(savedInstanceState);
        StringBuilder hintBuffer = new StringBuilder("感谢您对");
        hintBuffer.append(getResources().getString(R.string.app_name));
        hintBuffer.append("的信任，我们将按法律法规要求，采取严格的安全保护措施，保护您的个人隐私信息。在此，我们郑重的提醒您：");
        setText(R.id.tv_privacy_hint, hintBuffer.toString());

        String str = "1. 在您使用我方提供的服务时，建议您详细阅读《用户协议》和《用户隐私政策》，详细了解我方收集、存储、使用、披露和保护您的个人信息的举措，进而帮助您更好地保护您的隐私。";
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(str);

        String agreementStr = "《用户协议》";
        int agreementStart = str.indexOf(agreementStr);
        spannableStringBuilder.setSpan(new ClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {
                //用户协议
                if (mIPrivacyListener != null) {
                    mIPrivacyListener.onClickAgreement();
                } else {
                    create(R.string.agreement, PrivacyDao.agreement());
                }
            }

            @Override
            public void updateDrawState(@NonNull TextPaint ds) {
                ds.setColor(ColorUtils.getColor(R.color.colorAccent));
                ds.setUnderlineText(false);
            }
        }, agreementStart, agreementStart + agreementStr.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        String privacyStr = "《用户隐私政策》";
        int indexStart = str.indexOf(privacyStr);

        spannableStringBuilder.setSpan(new ClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {
                //隐私政策
                if (mIPrivacyListener != null) {
                    mIPrivacyListener.onClickPrivacy();
                } else {
                    create(R.string.privacy, PrivacyDao.privacy());
                }
            }

            @Override
            public void updateDrawState(@NonNull TextPaint ds) {
                ds.setColor(ColorUtils.getColor(R.color.colorAccent));
                ds.setUnderlineText(false);
            }
        }, indexStart, indexStart + privacyStr.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        setText(R.id.tv_privacy, spannableStringBuilder);
        getTextView(R.id.tv_privacy).setMovementMethod(LinkMovementMethod.getInstance());
        getTextView(R.id.tv_agree).setOnClickListener(this);
        getTextView(R.id.tv_exit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (_mActivity != null) {
                    _mActivity.finish();
                }
            }
        });
    }

    @Override
    public boolean onBackPressedSupport() {
        if (_mActivity != null) {
            _mActivity.finish();
        }
        return true;
    }

    /**
     * @param title:跳转到网页界面的标题
     * @param url:网页地址
     */
    protected void create(int title, String url) {
        start(WebUiDelegate.create(getResources().getString(title), url));
    }

    @Override
    public void onClick(View v) {
        PrivacyLauncher.setPrivacyLauncher();
        //检查用户是否已经登录
        AccountManager.checkAccount(new IUserChecker() {
            @Override
            public void onSignIn() {
                if (mLauncherListener != null) {
                    mLauncherListener.onLauncherFinish(OnLauncherFinishTag.SIGNED);
                }
            }

            @Override
            public void onNotSignIn() {
                if (mLauncherListener != null) {
                    mLauncherListener.onLauncherFinish(OnLauncherFinishTag.NOT_SIGNED);
                }
            }
        });
    }
}
