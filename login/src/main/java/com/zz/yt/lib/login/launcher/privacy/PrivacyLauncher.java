package com.zz.yt.lib.login.launcher.privacy;

import com.whf.android.jar.util.storage.LattePreference;

/**
 * 判断是否显示用户隐私保护
 *
 * @author 傅令杰
 * @version 2017/4/22
 */

public class PrivacyLauncher {


    private enum ScrollLauncherTag {
        /**
         * 保存否显示用户隐私保护
         */
        IS_PRIVACY
    }

    //region 显示滑动启动页

    /***
     * 判断是否显示
     * @return 是否:用户隐私保护
     */
    public static boolean isPrivacyLauncher() {
        return !LattePreference.getAppFlag(ScrollLauncherTag.IS_PRIVACY.name());
    }

    /**
     * 设置不显示:用户隐私保护
     */
    public static void setPrivacyLauncher() {
        setPrivacyLauncher(true);
    }

    /**
     * 设置是否显示滑动启动页
     *
     * @param flag:是否:用户隐私保护
     */
    public static void setPrivacyLauncher(boolean flag) {
        LattePreference.setAppFlag(ScrollLauncherTag.IS_PRIVACY.name(), flag);
    }
    //endregion
}
