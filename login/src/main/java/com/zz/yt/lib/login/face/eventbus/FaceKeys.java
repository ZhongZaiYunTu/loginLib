package com.zz.yt.lib.login.face.eventbus;

/**
 * @author qf
 * @author 中再云图技术有限公司
 * @version 1.0
 * @date 2020/10/26
 **/
public interface FaceKeys {

    /**
     * 识别间隔
     */
    int FACE = 200;

    /**
     * 8秒未识别成功，超时
     */
    int OVERTIME = 8000;

    /**
     * 相机旋转180度
     */
    int ROTATION_180 = 180;

    /**
     * 相机旋转270度
     */
    int ROTATION_270 = 270;
}
