package com.zz.yt.lib.login.delegate.login.base;

import android.text.TextUtils;

import com.blankj.utilcode.util.RegexUtils;
import com.blankj.utilcode.util.StringUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.whf.android.jar.constants.INumberConstant;
import com.whf.android.jar.constants.UserConstant;
import com.whf.android.jar.dao.UserDao;
import com.whf.android.jar.util.storage.LattePreference;
import com.zz.yt.lib.login.R;
import com.zz.yt.lib.ui.base.LatteDelegate;
import com.zz.yt.lib.ui.item.EditLayoutBar;


/**
 * 默认可以获得的值
 *
 * @author qf
 * @version 1.0
 **/
abstract class BaseDataDelegate extends LatteDelegate {


    /**
     * 再点一次退出程序时间设置
     */
    private static final long WAIT_TIME = 2000L;

    /**
     * 是否必须更新
     */
    private final static String GRANTED_IS = "grantedIS";

    /**
     * 上传退出时间
     */
    private long mTouchTime = 0;

    @Override
    protected boolean setMourning() {
        return true;
    }

    @Override
    public boolean onBackPressedSupport() {
        if (System.currentTimeMillis() - mTouchTime < WAIT_TIME) {
            _mActivity.finish();
        } else {
            mTouchTime = System.currentTimeMillis();
            ToastUtils.showShort("再次点击退出" + StringUtils.getString(R.string.app_name));
        }
        return true;
    }

    protected EditLayoutBar getEditTextBar(int viewId) {
        return findViewById(viewId);
    }

    /**
     * 设置选中
     *
     * @return 选中
     */
    protected boolean isSelect() {
        return LattePreference.getAppFlag(UserConstant.USER_IS);
    }

    /**
     * 保存选中
     *
     * @param select： 选中
     */
    protected void setSelect(boolean select) {
        LattePreference.setAppFlag(UserConstant.USER_IS, select);
    }

    /**
     * 需要更新
     *
     * @return 是
     */
    protected boolean isGranted() {
        return LattePreference.getAppFlag(GRANTED_IS);
    }

    /**
     * 设置需要更新
     *
     * @param select： 选中
     */
    void setGranted(boolean select) {
        LattePreference.setAppFlag(GRANTED_IS, select);
    }


    /**
     * 设置账号
     *
     * @return 账号
     */
    protected String getAccount() {
        if (!isSelect()) {
            return "";
        }
        return UserDao.getUserUser();
    }

    /**
     * 保存账号
     *
     * @param account： 账号
     */
    private void setAccount(String account) {
        UserDao.setUserUser(account);
    }

    /**
     * 判断账号
     *
     * @param account：账号
     * @return true
     */
    protected boolean isAccount(String account) {
        if (TextUtils.isEmpty(account)) {
            ToastUtils.showShort("请输入账号");
            return false;
        }
        setAccount(account);
        return true;
    }

    /**
     * 设置密码
     *
     * @return 密码
     */
    protected String getPassword() {
        if (!isSelect()) {
            return "";
        }
        return LattePreference.getCustomAppProfile(UserConstant.USER_PASS, "");
    }

    /**
     * 保存密码
     *
     * @param password： 密码
     */
    private void setPassword(String password) {
        LattePreference.addCustomAppProfile(UserConstant.USER_PASS, password);
    }

    /**
     * 判断密码
     *
     * @param password：密码
     * @return true
     */
    protected boolean isPassword(String password) {
        if (TextUtils.isEmpty(password)) {
            ToastUtils.showShort("请输入密码");
            return false;
        }
        if (password.length() < INumberConstant.SIX) {
            ToastUtils.showShort("请输入正确的密码");
            return false;
        }
        setPassword(password);
        return true;
    }

    /**
     * 获得手机号码
     *
     * @return 手机号码
     */
    protected String getPhone() {
        return LattePreference.getCustomAppProfile(UserConstant.USER_PHONE, "");
    }

    /**
     * 保存手机号码
     *
     * @param phone： 手机号码
     */
    private void setPhone(String phone) {
        LattePreference.addCustomAppProfile(UserConstant.USER_PHONE, phone);
    }

    /**
     * 判断手机号码
     *
     * @param phone：手机号码
     * @return true
     */
    protected boolean isPhone(String phone) {
        if (TextUtils.isEmpty(phone)) {
            ToastUtils.showShort("请输入手机号码");
            return false;
        }
        if (!RegexUtils.isMobileExact(phone)) {
            ToastUtils.showShort("请输入正确的手机号码");
            return false;
        }
        setPhone(phone);
        return true;
    }

    /**
     * 判断验证码
     *
     * @param code：验证码
     * @return true
     */
    protected boolean isCode(String code) {
        if (TextUtils.isEmpty(code)) {
            ToastUtils.showShort("请输入验证码");
            return false;
        }
        if (code.length() < INumberConstant.FOUR) {
            ToastUtils.showShort("请输入正确的验证码");
            return false;
        }
        return true;
    }


}
