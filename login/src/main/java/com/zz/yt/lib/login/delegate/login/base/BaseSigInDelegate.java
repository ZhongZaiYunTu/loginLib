package com.zz.yt.lib.login.delegate.login.base;


import android.app.KeyguardManager;
import android.hardware.fingerprint.FingerprintManager;
import android.text.Editable;
import android.text.TextWatcher;

import androidx.biometric.BiometricManager;
import androidx.biometric.BiometricManager.Authenticators;

import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.ObjectUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.whf.android.jar.app.Latte;
import com.zz.yt.lib.login.R;
import com.zz.yt.lib.login.checker.AccountManager;
import com.zz.yt.lib.login.checker.FingerManager;
import com.zz.yt.lib.login.utils.HarmonyUtils;
import com.zz.yt.lib.login.view.AgreementPrivacyView;
import com.zz.yt.lib.ui.delegate.web.WebUiDelegate;
import com.zz.yt.lib.ui.item.EditLayoutBar;
import com.zz.yt.lib.ui.privacy.IPrivacyListener;
import com.zz.yt.lib.ui.privacy.PrivacyDao;
import com.zz.yt.lib.ui.view.SelectImageView;


/**
 * 默认可以获得的值
 *
 * @author qf
 * @version 1.0
 **/
public abstract class BaseSigInDelegate extends BaseGradeDelegate implements TextWatcher {

    /**
     * 设置跳转到用户协议和隐私政策
     */
    protected void agreementPrivacy() {
        AgreementPrivacyView agreementPrivacyView = findViewById(R.id.agreement_privacy_view);
        if (agreementPrivacyView != null) {
            agreementPrivacyView.setIPrivacyListener(new IPrivacyListener() {
                @Override
                public void onClickAgreement() {
                    //用户协议
                    if (mIPrivacyListener != null) {
                        mIPrivacyListener.onClickAgreement();
                    } else {
                        create(R.string.agreement, PrivacyDao.agreement());
                    }
                }

                @Override
                public void onClickPrivacy() {
                    //隐私政策
                    if (mIPrivacyListener != null) {
                        mIPrivacyListener.onClickPrivacy();
                    } else {
                        create(R.string.privacy, PrivacyDao.privacy());
                    }
                }
            });
        }
    }

    /**
     * 设置是否保存登录信息
     */
    protected void setImgSelect(SelectImageView idImageSelect) {
        if (idImageSelect != null) {
            idImageSelect.setChecked(isSelect());
            idImageSelect.setOnClickListener(v -> {
                idImageSelect.select();
                setSelect(idImageSelect.isChecked());
            });
        }
        //登录状态为未登录
        AccountManager.setSignState(false);
    }

    /**
     * @param title:跳转到网页界面的标题
     * @param url:网页地址
     */
    protected void create(int title, String url) {
        start(WebUiDelegate.create(getResources().getString(title), url));
    }

    /**
     * @return 是否支持指纹功能
     */
    protected boolean supportFingerprint() {
        boolean res = false;
        if (!FingerManager.isFinger()) {
            //是否开通指纹登录。
            return false;
        }
        if (HarmonyUtils.isHarmonyOS()) {
            //Android6~Android9的指纹识别认证 鸿蒙兼容
            KeyguardManager keyguardManager = Latte.getApplicationContext()
                    .getSystemService(KeyguardManager.class);
            FingerprintManager fingerprintManager = Latte.getApplicationContext()
                    .getSystemService(FingerprintManager.class);
            if (ObjectUtils.isEmpty(fingerprintManager) || ObjectUtils.isEmpty(keyguardManager)) {
                ToastUtils.showShort("指纹功能异常");
                res = false;
            } else if (!fingerprintManager.isHardwareDetected()) {
                ToastUtils.showShort("您的手机不支持指纹功能");
                res = false;
            } else if (!keyguardManager.isKeyguardSecure()) {
                ToastUtils.showShort("您还未设置锁屏，请先设置锁屏并添加一个指纹");
                res = false;
            } else if (!fingerprintManager.hasEnrolledFingerprints()) {
                ToastUtils.showShort("您至少需要在系统设置中添加一个指纹");
                res = false;
            } else {
                res = true;
            }
        } else {
            //Android6以上的生物识别认证
            int status = BiometricManager.from(Latte.getApplicationContext())
                    .canAuthenticate(Authenticators.DEVICE_CREDENTIAL);

            switch (status) {
                case BiometricManager.BIOMETRIC_SUCCESS:
                    LogUtils.d("BIOMETRIC_SUCCESS");
                    res = true;
                    break;
                case BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE:
                    LogUtils.d("BIOMETRIC_ERROR_NO_HARDWARE");
                    ToastUtils.showShort("您的手机不支持指纹功能");
                    break;
                case BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE:
                    LogUtils.d("BIOMETRIC_ERROR_HW_UNAVAILABLE");
                    ToastUtils.showShort("您的手机相关硬件不可用");
                    break;
                case BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED:
                    LogUtils.d("BIOMETRIC_ERROR_NONE_ENROLLED");
                    ToastUtils.showShort("您至少需要在系统设置中添加一个指纹");
                    break;
                case BiometricManager.BIOMETRIC_ERROR_SECURITY_UPDATE_REQUIRED:
                    LogUtils.d("BIOMETRIC_ERROR_SECURITY_UPDATE_REQUIRED");
                    ToastUtils.showShort("相关硬件传感器存在漏洞，该功能不可用");
                    break;
                case BiometricManager.BIOMETRIC_ERROR_UNSUPPORTED:
                    LogUtils.d("BIOMETRIC_ERROR_UNSUPPORTED");
                    ToastUtils.showShort("指定功能与当前Android版本不兼容，该功能不可用");
                    break;
                case BiometricManager.BIOMETRIC_STATUS_UNKNOWN:
                    LogUtils.d("BIOMETRIC_STATUS_UNKNOWN");
                    ToastUtils.showShort("功能异常");
                    break;
                default:
                    break;
            }
        }
        return res;
    }

    /**
     * @param mPassword:密码显示隐藏
     */
    protected void inPassword(EditLayoutBar mPassword) {
        if (mPassword == null) {
            return;
        }
        mPassword.setText(getPassword());
        mPassword.getValueView().addTextChangedListener(this);
        mPassword.setPassword(R.drawable.login_ic_visibility_off,
                R.drawable.login_ic_visibility_on);
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable s) {

    }

}
