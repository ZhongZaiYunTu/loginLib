package com.zz.yt.lib.login.utils;

import android.text.TextUtils;
import android.view.View;

import com.blankj.utilcode.util.ObjectUtils;
import com.blankj.utilcode.util.RegexUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.whf.android.jar.app.Latte;
import com.whf.android.jar.constants.INumberConstant;
import com.whf.android.jar.util.callback.CallbackManager;
import com.whf.android.jar.util.callback.CallbackType;
import com.whf.android.jar.util.callback.IGlobalCallback;
import com.whf.android.jar.util.log.LatteLogger;
import com.whf.android.jar.util.number.DoubleUtils;
import com.zz.yt.lib.login.badge.BadgeUtils;
import com.zz.yt.lib.login.checker.FingerManager;
import com.zz.yt.lib.login.popup.FingerPopup;

/**
 * 登录工具类
 *
 * @author hf
 * @version 1.0.2
 */
public final class LoginUtils {


    /**
     * 判断手机号码
     *
     * @param phone：手机号码
     * @return true
     */
    public static boolean isPhone(String phone) {
        if (TextUtils.isEmpty(phone)) {
            ToastUtils.showShort("请输入手机号码");
            return false;
        }
        if (phone.length() != 11 || !RegexUtils.isMobileExact(phone)) {
            ToastUtils.showShort("请输入正确的手机号码");
            return false;
        }
        return true;
    }

    /**
     * 判断验证码
     *
     * @param code：验证码
     * @return true
     */
    public static boolean isCode(String code) {
        if (TextUtils.isEmpty(code)) {
            ToastUtils.showShort("请输入验证码");
            return false;
        }
        if (!(code.length() == INumberConstant.FOUR || code.length() == INumberConstant.SIX)) {
            ToastUtils.showShort("请输入正确的验证码");
            return false;
        }
        return true;
    }

    /**
     * 判断密码
     *
     * @param pass:新密码
     * @param confirmPass:确认密码
     */
    public static boolean isPass(String pass, String confirmPass) {
        if (ObjectUtils.isEmpty(pass)) {
            ToastUtils.showShort("请输入新密码");
            return false;
        }
        if (ObjectUtils.isEmpty(confirmPass)) {
            ToastUtils.showShort("请输入确认密码");
            return false;
        }
        if (!pass.equals(confirmPass)) {
            ToastUtils.showShort("确认密码与新密码不相同");
            return false;
        }
        if (RegexUtils.isZh(pass)) {
            ToastUtils.showShort("新密码不能有汉字");
            return false;
        }
        if (pass.length() < INumberConstant.SIX) {
            ToastUtils.showShort("新密码不能小于6位");
            return false;
        }
        return true;
    }

    /**
     * @param cla:初始化角标设置
     */
    public static void initBadge(Class cla) {
        initBadge(cla, Latte.getDebug());
    }

    /**
     * @param cla:初始化角标设置
     * @param bug:是否开启角标的日志
     */
    public static void initBadge(Class cla, boolean bug) {
        CallbackManager.getInstance().addCallback(CallbackType.TIDINGS, args -> {
            int messageCount = DoubleUtils.objToInt(args);
            int message = Math.max(0, messageCount);
            BadgeUtils.Bundle(cla)
                    .setContentTitle("信息")
                    .setContentText("您有新的信息")
                    .setBug(bug)
                    .init(message);
        });
    }

    /**
     * @param data:设置角标的显示数
     */
    public static void setBadge(int data) {
        //刷新角标
        @SuppressWarnings("unchecked") final IGlobalCallback<Integer> callback =
                CallbackManager.getInstance()
                        .getCallback(CallbackType.TIDINGS);
        if (callback != null) {
            callback.executeCallback(data);
        }
    }

    /**
     * 提示是否开启指纹登录
     */
    public static void setFinger(View.OnClickListener listener) {
        if (FingerManager.isFinger()) {
            LatteLogger.i("已开通指纹登录");
            if (listener != null) {
                listener.onClick(null);
            }
            return;
        }
        FingerPopup.create(listener);
    }

}
