package com.zz.yt.lib.login.delegate.debug;


import android.widget.TextView;

import androidx.annotation.NonNull;

import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.JsonUtils;
import com.whf.android.jar.app.Latte;
import com.whf.android.jar.constants.AppConstant;
import com.whf.android.jar.constants.HttpConstant;
import com.whf.android.jar.net.HttpCode;
import com.whf.android.jar.net.RestClient;
import com.whf.android.jar.util.log.LatteLogger;

import java.util.List;


/**
 * 切换服务(debug)
 *
 * @author qf
 * @version 1.0
 **/
public class LoginDebugUtils extends BaseLoginUtils {

    private final int mPort;

    @NonNull
    public static LoginDebugUtils create(TextView textView, int port) {
        return new LoginDebugUtils(textView, port);
    }

    protected LoginDebugUtils(TextView textView, int port) {
        super(textView);
        this.mPort = port;
        //设置服务
        jsonArray.clear();
        jsonArray.add(new PickerEntry("默认服务", Latte.getServer()));
        RestClient.builder()
                .url(AppConstant.TEST_SERVICE_NAME)
                .success(response -> {
                    LatteLogger.json(response);
                    if (HttpCode.CODE_200 == JsonUtils.getInt(response, HttpConstant.CODE)) {
                        final String data = JsonUtils.getString(response, HttpConstant.DATA);
                        List<PickerEntry> mArray = GsonUtils.fromJson(data, GsonUtils.getListType(PickerEntry.class));
                        if (mArray != null) {
                            for (PickerEntry entry : mArray) {
                                entry.setComments(entry.getComments() + mPort);
                                jsonArray.add(entry);
                            }
                        }
                        setText(options);
                    }
                })
                .build()
                .get();
    }

}
