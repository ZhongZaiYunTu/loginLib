package com.zz.yt.lib.login.face.utils;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.media.FaceDetector;

import com.blankj.utilcode.util.ImageUtils;

import java.io.ByteArrayOutputStream;

import io.fotoapparat.preview.Frame;

/**
 * 人脸检测工具类
 *
 * @author lzl
 * @date 2021/9/5
 */
public class FaceDetectorUtils {

    private final static int MAX_FACES = 10;

    private FaceDetectorUtils() {
    }

    public interface Callback {
        /**
         * 检测到面部
         *
         * @param faces:
         * @param bitmap:
         */
        void onFaceDetected(FaceDetector.Face[] faces, Bitmap bitmap);

        /**
         * 未检测到面部
         */
        void onFaceNotDetected();
    }

    /**
     * 检测frame中的人脸，在callback中返回人脸数据
     *
     * @param frame:
     * @param callback:
     */
    public static void detectFace(Frame frame, Callback callback) {
        try {
            final FaceDetector faceDetector = new FaceDetector(frame.getSize().width, frame.getSize().height, MAX_FACES);
            final FaceDetector.Face[] faces = new FaceDetector.Face[MAX_FACES];
            //ImageFormat.NV21  640 480
            YuvImage image = new YuvImage(frame.getImage(), ImageFormat.NV21, frame.getSize().width, frame.getSize().height, null);
            ByteArrayOutputStream outputSteam = new ByteArrayOutputStream();
            // 将NV21格式图片，以质量70压缩成Jpeg，并得到JPEG数据流
            image.compressToJpeg(new Rect(0, 0, image.getWidth(), image.getHeight()), 70, outputSteam);
            //从outputSteam得到Bitmap数据
            Bitmap bitmap = BitmapFactory.decodeByteArray(outputSteam.toByteArray(), 0, outputSteam.size());
            //图片格式转换
            bitmap = bitmap.copy(Bitmap.Config.RGB_565, true);
            int faceNum = faceDetector.findFaces(bitmap, faces);
            if (faceNum > 0) {
                if (callback != null) {
                    callback.onFaceDetected(faces, bitmap);
                }
            } else {
                if (callback != null) {
                    callback.onFaceNotDetected();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (callback != null) {
                callback.onFaceNotDetected();
            }
        }
    }

    /**
     * 检测bitmap中的人脸，在callback中返回人脸数据
     *
     * @param bitmap:
     * @param callback:
     */
    public static void detectFace(Bitmap bitmap, Callback callback) {
        try {
            final FaceDetector faceDetector = new FaceDetector(bitmap.getWidth(), bitmap.getHeight(), MAX_FACES);
            FaceDetector.Face[] faces = new FaceDetector.Face[MAX_FACES];
            int faceNum = faceDetector.findFaces(ImageUtils.toAlpha(bitmap), faces);
            if (faceNum > 0) {
                if (callback != null) {
                    callback.onFaceDetected(faces, bitmap);
                }
            } else {
                if (callback != null) {
                    callback.onFaceNotDetected();
                    bitmap.recycle();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (callback != null) {
                callback.onFaceNotDetected();
            }
        }
    }

}