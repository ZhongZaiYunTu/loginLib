package com.zz.yt.lib.login.view;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.blankj.utilcode.util.KeyboardUtils;
import com.whf.android.jar.app.Latte;
import com.zz.yt.lib.login.R;
import com.zz.yt.lib.ui.view.base.BaseLinearLayout;

import java.util.ArrayList;
import java.util.Collection;

/**
 * 切换服务(正式)
 *
 * @author qf
 * @author 中再云图技术有限公司
 * @version 1.0.24
 **/
public class ButtonCloudView extends BaseLinearLayout implements TextWatcher {

    private final EditText idEditAccount, idEditPassword;
    private final LinearLayout lLayoutLogin;
    private final RelativeLayout rl1, rl2, rl3, rl4;
    private final Button loginSign;
    private final Collection<Animator> animators = new ArrayList<>();
    private final ObjectAnimator llLoginAlpha;
    private int type = 0;
    private ICloudListener mCloudListener = null;

    @Override
    protected int setLayout() {
        return R.layout.hai_view_cloud;
    }

    public ButtonCloudView(Context context, AttributeSet attrs) {
        super(context, attrs);
        idEditAccount = findViewById(R.id.id_edit_account);
        idEditPassword = findViewById(R.id.id_edit_password);
        idEditPassword.addTextChangedListener(this);

        lLayoutLogin = findViewById(R.id.ll_login);
        loginSign = findViewById(R.id.id_btn_sign_in);

        llLoginAlpha = ObjectAnimator.ofFloat(lLayoutLogin, View.ALPHA, 0.5f, 1.0F);
        llLoginAlpha.setDuration(150);
        llLoginAlpha.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                lLayoutLogin.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animation) {

            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

        setOnClickListener(R.id.id_text_server_1, v -> {
            type = 0;
            show();
        });
        setOnClickListener(R.id.id_text_server_2, v -> {
            type = 1;
            show();
        });
        setOnClickListener(R.id.id_text_server_3, v -> {
            type = 2;
            show();
        });
        setOnClickListener(R.id.id_text_server_4, v -> {
            type = 3;
            show();
        });


        setOnClickListener(R.id.iv_back, v -> hide());
        setOnClickListener(R.id.id_btn_sign_in, v -> {
            String account = idEditAccount.getText().toString().trim();
            String password = idEditPassword.getText().toString().trim();
            if (mCloudListener != null) {
                mCloudListener.onCloud(account, password);
            }
        });

        rl1 = findViewById(R.id.rl_1);
        rl2 = findViewById(R.id.rl_2);
        rl3 = findViewById(R.id.rl_3);
        rl4 = findViewById(R.id.rl_4);

    }

    /**
     * @param account:回填保持的账号
     * @param password：回填保持的密码
     */
    public void onCloud(String account, String password) {
        if (idEditAccount != null) {
            idEditAccount.setText(account);
        }
        if (idEditPassword != null) {
            idEditPassword.setText(password);
        }
    }

    public void setTypeIcon(int type, int code) {
        if (type == 0) {
            setTypeIcon1(code);
        } else if (type == 1) {
            setTypeIcon2(code);
        } else if (type == 2) {
            setTypeIcon3(code);
        } else if (type == 3) {
            setTypeIcon4(code);
        }
    }

    public void setTypeIcon1(int code) {
        setText(R.id.id_tv_icon_1, code + "");
        setShow(R.id.id_tv_icon_1, code > 0);
    }

    public void setTypeIcon2(int code) {
        setText(R.id.id_tv_icon_2, code + "");
        setShow(R.id.id_tv_icon_2, code > 0);
    }

    public void setTypeIcon3(int code) {
        setText(R.id.id_tv_icon_3, code + "");
        setShow(R.id.id_tv_icon_3, code > 0);
    }

    public void setTypeIcon4(int code) {
        setText(R.id.id_tv_icon_4, code + "");
        setShow(R.id.id_tv_icon_4, code > 0);
    }

    public void show() {
        if (mClickIntObjListener != null) {
            if (0 == type) {
                setText(R.id.tv_title,"供销环境");
                mClickIntObjListener.onClick(type, "供销环境");
            } else if (1 == type) {
                setText(R.id.tv_title,"中再控股");
                mClickIntObjListener.onClick(type, "中再控股");
            } else if (2 == type) {
                setText(R.id.tv_title,"中再环科");
                mClickIntObjListener.onClick(type, "中再环科");
            } else if (3 == type) {
                setText(R.id.tv_title,"新网工程");
                mClickIntObjListener.onClick(type, "新网工程");
            }
        }

        float moveLength = lLayoutLogin.getY() + lLayoutLogin.getHeight() - rl4.getY() - 250;

        ObjectAnimator oaTv1HideMove = ObjectAnimator.ofFloat(rl1, View.TRANSLATION_Y, 0.0f, moveLength + 20);
        ObjectAnimator oaTv1HideRota = ObjectAnimator.ofFloat(rl1, View.ROTATION_X, 0, -90);

        ObjectAnimator oaTv2HideMove = ObjectAnimator.ofFloat(rl2, View.TRANSLATION_Y, 0.0f, moveLength + 20);
        ObjectAnimator oaTv2HideRota = ObjectAnimator.ofFloat(rl2, View.ROTATION_X, 0, -90);

        ObjectAnimator oaTv3HideMove = ObjectAnimator.ofFloat(rl3, View.TRANSLATION_Y, 0.0f, moveLength + 20);
        ObjectAnimator oaTv3HideRota = ObjectAnimator.ofFloat(rl3, View.ROTATION_X, 0, -90);

        ObjectAnimator oaTv4HideMove = ObjectAnimator.ofFloat(loginSign, View.TRANSLATION_Y, 0.0f, moveLength);
//        ObjectAnimator oaTv4HideRota = ObjectAnimator.ofFloat(tv3, "rotationX", 0, -90);


        animators.clear();
        animators.add(oaTv1HideMove);
        animators.add(oaTv1HideRota);
        animators.add(oaTv2HideMove);
        animators.add(oaTv2HideRota);
        animators.add(oaTv3HideMove);
        animators.add(oaTv3HideRota);
        animators.add(oaTv4HideMove);
//        animators.add(oaTv4HideRota);

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(animators);
        animatorSet.setDuration(250);
        animatorSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                rl4.setVisibility(View.INVISIBLE);
                loginSign.setVisibility(View.VISIBLE);
                Latte.getHandler().postDelayed(() -> KeyboardUtils.showSoftInput(idEditAccount), 150);

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                llLoginAlpha.start();
                rl1.setVisibility(View.INVISIBLE);
                rl2.setVisibility(View.INVISIBLE);
                setShow(R.id.iv_back, true);
                KeyboardUtils.showSoftInput(idEditAccount);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        animatorSet.start();
    }

    public void hide() {
        animators.clear();
        KeyboardUtils.hideSoftInput(Latte.getActivity());

        ObjectAnimator oaTv1HideMove = ObjectAnimator.ofFloat(rl1, View.TRANSLATION_Y, rl1.getTranslationY(), 0.0f);
        ObjectAnimator oaTv1HideRota = ObjectAnimator.ofFloat(rl1, View.ROTATION_X, -90, 0);

        ObjectAnimator oaTv2HideMove = ObjectAnimator.ofFloat(rl2, View.TRANSLATION_Y, rl2.getTranslationY(), 0.0f);
        ObjectAnimator oaTv2HideRota = ObjectAnimator.ofFloat(rl2, View.ROTATION_X, -90, 0);

        ObjectAnimator oaTv3HideMove = ObjectAnimator.ofFloat(rl3, View.TRANSLATION_Y, rl3.getTranslationY(), 0.0f);
        ObjectAnimator oaTv3HideRota = ObjectAnimator.ofFloat(rl3, View.ROTATION_X, -90, 0);

        ObjectAnimator oaTv4HideMove = ObjectAnimator.ofFloat(rl4, View.TRANSLATION_Y, loginSign.getTranslationY(), 0.0f);
//        ObjectAnimator oaTv4HideRota = ObjectAnimator.ofFloat(tv3, "rotationX", -90, 0);

        animators.add(oaTv1HideMove);
        animators.add(oaTv1HideRota);
        animators.add(oaTv2HideMove);
        animators.add(oaTv2HideRota);
        animators.add(oaTv3HideMove);
        animators.add(oaTv3HideRota);
        animators.add(oaTv4HideMove);
//        animators.add(oaTv4HideRota);

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(animators);
        animatorSet.setDuration(300);
        animatorSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                rl1.setVisibility(View.VISIBLE);
                rl2.setVisibility(View.VISIBLE);
                rl3.setVisibility(View.VISIBLE);
                rl4.setVisibility(View.VISIBLE);
                setShow(R.id.iv_back, false);
                loginSign.setVisibility(View.INVISIBLE);
                lLayoutLogin.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationEnd(Animator animation) {

            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

        animatorSet.start();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (idEditPassword != null && loginSign != null) {
            loginSign.setEnabled(idEditPassword.getText().toString().trim().length() > 1);
        }
    }

    public void setOnCloudListener(ICloudListener listener) {
        this.mCloudListener = listener;
    }

    public interface ICloudListener {


        /**
         * @param account:账号
         * @param password:密码
         */
        void onCloud(String account, String password);

    }

}
