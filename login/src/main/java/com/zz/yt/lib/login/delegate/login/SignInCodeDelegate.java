package com.zz.yt.lib.login.delegate.login;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.zz.yt.lib.login.R;
import com.zz.yt.lib.login.checker.AccountManager;
import com.zz.yt.lib.login.checker.AgreementPrivacyManager;
import com.zz.yt.lib.login.constant.ISignCodeListener;
import com.zz.yt.lib.login.delegate.login.base.BaseSigInDelegate;
import com.zz.yt.lib.ui.listener.OnClickBoolListener;
import com.zz.yt.lib.ui.view.phone.PhoneCodeView;


/**
 * 手机号和验证码登录
 *
 * @author qf
 * @version 20.1.5
 */
public class SignInCodeDelegate extends BaseSigInDelegate implements OnClickBoolListener {


    private Button loginSign;
    private PhoneCodeView phoneCodeView;
    private ISignCodeListener mSignCodeListener = null;

    @Override
    public void onAttach(@NonNull Context activity) {
        super.onAttach(activity);
        if (activity instanceof ISignCodeListener) {
            mSignCodeListener = (ISignCodeListener) activity;
        }
    }

    @Override
    public Object setLayout() {
        return R.layout.hai_delegate_sign_in_code;
    }

    private void inBundle() {
        if (mSignCodeListener != null) {
            Object launcher = mSignCodeListener.setSignIn();
            if (launcher != null) {
                if (launcher instanceof Integer) {
                    getImageView(R.id.login_image_background)
                            .setImageResource((int) launcher);
                } else if (launcher instanceof View) {
                    View viewLauncher = (View) launcher;
                    RelativeLayout view = findViewById(R.id.login_layout_background);
                    ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.MATCH_PARENT);
                    viewLauncher.setLayoutParams(layoutParams);
                    view.addView(viewLauncher);
                }
            }
        }
        //登录状态为未登录
        AccountManager.setSignState(false);
    }

    @Override
    public void onBindView(@Nullable Bundle savedInstanceState, @NonNull View rootView) {
        inBundle();
        initView();
        inGradePermission();
    }

    private void initView() {
        phoneCodeView = findViewById(R.id.id_phone_code_view);
        phoneCodeView.setOnClickBoolListener(this);
        loginSign = getButton(R.id.login_btn_sign_in);

        //设置跳转到用户协议和隐私政策
        agreementPrivacy();

        //登录按钮设置
        if (mSignCodeListener != null) {
            mSignCodeListener.setSignInView(loginSign);
        }
    }


    @Override
    protected void onGrantedSuccess() {
        if (phoneCodeView != null) {
            phoneCodeView.setOnClickStringListener(phone -> {
                if (mSignCodeListener != null) {
                    mSignCodeListener.onPhoneCode(phone);
                }
            });
        }
        loginSign.setOnClickListener(view -> {
            if (isGranted()) {
                setInstallPermission();
                return;
            }
            if (AgreementPrivacyManager.is()) {
                return;
            }
            if (mSignCodeListener != null && phoneCodeView != null) {
                String phone = phoneCodeView.getPhone();
                String code = phoneCodeView.getPhoneCode();
                if (isPhone(phone) && isCode(code)) {
                    phoneCodeView.setTiming(true);
                    mSignCodeListener.onPhoneSignIn(phone, code);
                }
            }
        });
    }

    @Override
    public void onClick(boolean enabled) {
        if (loginSign != null) {
            loginSign.setEnabled(enabled);
        }
    }
}
