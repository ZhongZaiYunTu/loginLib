package com.zz.yt.lib.login.grade;

import android.content.Context;
import android.view.View;

import com.blankj.utilcode.util.StringUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.blankj.utilcode.util.ViewUtils;
import com.whf.android.jar.app.Latte;
import com.whf.android.jar.net.RestClient;
import com.whf.android.jar.net.callback.IRequest;
import com.whf.android.jar.popup.listener.OnClickDecisionListener;
import com.whf.android.jar.util.log.LatteLogger;
import com.zz.yt.lib.login.R;
import com.zz.yt.lib.login.grade.bean.GradeBean;

import org.jetbrains.annotations.NotNull;


/**
 * 升级版本
 *
 * @author qf
 * @version 22.3.12
 */
public class UpDateUtils implements OnClickDecisionListener {

    private final UpDatePopup mComplexPopup;
    private GradeBean mGradeBean = null;
    private View.OnClickListener mClickListener = null;

    private UpDateUtils(Context context) {
        this.mComplexPopup = UpDatePopup.create(context);
    }

    @NotNull
    public static UpDateUtils create() {
        return new UpDateUtils(Latte.getActivity());
    }

    public UpDateUtils setDate(GradeBean gradeBean) {
        this.mGradeBean = gradeBean;
        return this;
    }

    public UpDateUtils setOnClickListener(View.OnClickListener listener) {
        this.mClickListener = listener;
        return this;
    }

    public void show() {
        if (mGradeBean != null) {
            mComplexPopup.setGradeBean(mGradeBean)
                    .setOnClickListener(this)
                    .popup();
        }
    }


    @Override
    public void onConfirm(Object decision) {
        if (mGradeBean != null) {
            LatteLogger.i("开始更新，下载地址: " + mGradeBean.getApkPath());
            RestClient.builder()
                    .url(mGradeBean.getApkPath())
                    .name(StringUtils.getString(R.string.apk_name))
                    .failure(this::dismiss)
                    .error((code, msg) -> {
                        dismiss();
                        LatteLogger.e(msg == null ? "下载失败，请稍后再试。" : msg);
                        ToastUtils.showShort(msg == null ? "下载失败，请稍后再试。" : msg);
                    })
                    .onRequest(new IRequest() {
                        @Override
                        public void onRequestStart() {

                        }

                        @Override
                        public void onRequestProgress(int progress) {
                            ViewUtils.runOnUiThread(() -> setProgressBar(progress));
                        }

                        @Override
                        public void onRequestEnd() {
                            dismiss();
                            if (mClickListener != null) {
                                mClickListener.onClick(null);
                            }
                        }
                    })
                    .build()
                    .download();
        }

    }


    @Override
    public void onCancel() {
        LatteLogger.i("点击确定，开始更新");
        if (mClickListener != null) {
            mClickListener.onClick(null);
        }
    }

    private void setProgressBar(int progress) {
        if (mComplexPopup != null) {
            mComplexPopup.setProgressBar(progress);
        }
    }

    private void dismiss() {
        if (mComplexPopup != null) {
            mComplexPopup.dismiss();
        }
    }

}
