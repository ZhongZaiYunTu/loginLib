package com.zz.yt.lib.login.delegate.debug;

import com.contrarywind.interfaces.IPickerViewData;


public class PickerEntry implements IPickerViewData {


    /**
     * comments : http://10.35.11.208:
     * id : 345b0eb7e88f4de8b7b8ce1be02f2bbd
     * name : 姚老师
     * value : 1
     */

    private String comments;
    private String id;
    private String name;
    private String value;

    public PickerEntry(String name, String comments) {
        this.name = name;
        this.comments = comments;
    }

    @Override
    public String getPickerViewText() {
        return name;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}