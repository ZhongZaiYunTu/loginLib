package com.zz.yt.lib.login.badge;

import androidx.annotation.Nullable;

import com.blankj.utilcode.util.ThreadUtils;
import com.zz.yt.lib.ui.listener.OnClickIntListener;

import java.util.concurrent.TimeUnit;

/**
 * 刷新角标工具
 *
 * @author qf
 * @version 1.0.2
 */
public final class BadgeTimingUtils {

    /*** 角标刷新频率（秒） */
    private final int space;

    /*** 计时结束回调 */
    private final OnClickIntListener mClickIntListener;

    private int suspend;

    /*** 是否可以计时 */
    private boolean isTiming = false;

    /*** 是否调用后台刷新角标 */
    private static BadgeTimingUtils timing;

    /**
     * @param space:角标刷新频率（秒）
     * @param listener:计时结束回调。
     */
    public static BadgeTimingUtils create(int space, OnClickIntListener listener) {
        if (timing == null) {
            timing = new BadgeTimingUtils(space, listener);
        }
        return timing;
    }

    /**
     * @param space:角标刷新频率（秒）
     * @param listener:计时结束回调。
     */
    public BadgeTimingUtils(int space, OnClickIntListener listener) {
        this.space = space;
        this.mClickIntListener = listener;
        if (listener != null) {
            listener.onClick(suspend);
        }
        init();
    }

    /**
     * 开始计时
     */
    private void init() {
        suspend = space;
        if (isTiming) {
            //已经计时，不用在计时了。
            return;
        }
        isTiming = true;
        ThreadUtils.executeByFixedAtFixRate(1, new ThreadUtils.SimpleTask<String>() {

            @Override
            public String doInBackground() {
                return "";
            }

            @Override
            public void onSuccess(@Nullable String result) {
                suspend--;
                if (suspend < 0) {
                    suspend = space;
                    if (mClickIntListener != null) {
                        mClickIntListener.onClick(suspend);
                    }
                }
            }
        }, 1, TimeUnit.SECONDS);
    }

}
