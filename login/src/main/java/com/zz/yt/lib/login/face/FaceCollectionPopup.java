package com.zz.yt.lib.login.face;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.FaceDetector;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.whf.android.jar.app.Latte;
import com.whf.android.jar.popup.LattePopupCenter;
import com.whf.android.jar.util.log.LatteLogger;
import com.zz.yt.lib.login.R;
import com.zz.yt.lib.login.face.utils.FaceDetectorUtils;
import com.zz.yt.lib.login.face.utils.MediaRecorderUtils;

import org.jetbrains.annotations.NotNull;

import io.fotoapparat.preview.Frame;

import static com.blankj.utilcode.util.ViewUtils.runOnUiThread;

/**
 * 人脸采集
 *
 * @author qf
 * @version 1.0
 * @date 2020/9/4
 **/
public class FaceCollectionPopup extends BaseFacePopup {

    /**
     * false;请眨眼
     * true；请张嘴
     */
    private boolean isActive = false;
    private TextView activeView = null;
    private MediaRecorderUtils mediaRecorder = null;

    @NotNull
    public static FaceCollectionPopup create(Context context) {
        return new FaceCollectionPopup(context);
    }

    private FaceCollectionPopup(Context context) {
        super(context);
    }

    @Override
    protected int setLayout() {
        return R.layout.hai_popup_face_collection;
    }

    @Override
    protected void initViews() {
        activeView = findViewById(R.id.login_text_active);
        //1.一直波纹当着
        //2.检查到人脸，环形进度加载。（如果有4个检查动作，分成5等份，让圆环一直不满。）
        //3.调用接口，成功关闭界面。
        setActive();
    }

    public void setActive() {
        activeView.setText("请张嘴");
        isDetectingFace = true;
        isActive = true;
    }

    @Override
    public void process(@NotNull Frame frame) {
        super.process(frame);
        if (isDetectingFace) {
            LatteLogger.i("显示摄像头");
            return;
        }
        isDetectingFace = true;
        FaceDetectorUtils.detectFace(frame, new FaceDetectorUtils.Callback() {
            @Override
            public void onFaceDetected(FaceDetector.Face[] faces, Bitmap bitmap) {
                isDetectingFace = false;
                if (mediaRecorder == null) {
                    mediaRecorder = new MediaRecorderUtils(Latte.getActivity());
                    mediaRecorder.setCallback(videoFile -> {
                        if (mClickFaceListener != null) {
//                            runOnUiThread(() -> mClickFaceListener.onConfirm(videoFile, activeView.getText().toString()));
//                            if (isActive) {
//                                close();
//                            }
                        }
                    });
                }
                mediaRecorder.start();
            }

            @Override
            public void onFaceNotDetected() {
                isDetectingFace = false;
                LatteLogger.i("未检查到人脸");
                overtime();
            }
        });
    }
}
