package com.zz.yt.lib.login.delegate.login;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.zz.yt.lib.annex.utils.HeadUtils;
import com.zz.yt.lib.login.R;
import com.zz.yt.lib.login.checker.AccountManager;
import com.zz.yt.lib.login.constant.ILoginListener;
import com.zz.yt.lib.login.constant.ISignFaceListener;
import com.zz.yt.lib.login.delegate.login.base.BaseFingerDelegate;

/**
 * 指纹认证（获取存储的账号，刷新token）
 *
 * @author qf
 * @version 1.0.9
 **/
public class SignInFingerDelegate extends BaseFingerDelegate {

    private Button loginSign = null;
    private ILoginListener mLoginListener = null;
    private ISignFaceListener mSignFaceListener = null;


    @Override
    public Object setLayout() {
        return R.layout.hai_delegate_sign_in_finger;
    }


    @Override
    public void onAttach(@NonNull Context activity) {
        super.onAttach(activity);
        //这里是类型转换，而不是判断
        if (activity instanceof ILoginListener) {
            mLoginListener = (ILoginListener) activity;
        }

        if (activity instanceof ISignFaceListener) {
            mSignFaceListener = (ISignFaceListener) activity;
        }
    }

    private void inBundle() {
        if (mLoginListener != null) {
            Object launcher = mLoginListener.setSignIn();
            if (launcher != null) {
                if (launcher instanceof Integer) {
                    getImageView(R.id.login_image_background)
                            .setImageResource((int) launcher);
                } else if (launcher instanceof View) {
                    View viewLauncher = (View) launcher;
                    RelativeLayout view = findViewById(R.id.login_layout_background);
                    ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.MATCH_PARENT);
                    viewLauncher.setLayoutParams(layoutParams);
                    view.addView(viewLauncher);
                }
            }
        }
        //登录状态为未登录
        AccountManager.setSignState(false);
    }

    @Override
    public void onBindView(@Nullable Bundle savedInstanceState, @NonNull View rootView) {
        inBundle();
        initView();
        inGradePermission();
    }

    private void initView() {
        //头像
        HeadUtils.preview(getImageView(R.id.login_image_portrait));
        //账号
        getTextView(R.id.login_text_phone).setText(getPhone());
        //登录按钮
        loginSign = getButton(R.id.login_btn_sign_in);
        if (mLoginListener != null) {
            mLoginListener.setSignInView(loginSign);
        }
        loginSign.setEnabled(getAccount().length() > 1);


        //切换登录方式
        final Button loginSignCut = getButton(R.id.login_btn_sign_in_cut);
        if (mSignFaceListener != null) {
            mSignFaceListener.setSignInCutView(loginSignCut);
        }
        loginSignCut.setOnClickListener(v -> {
            if (mSignFaceListener != null) {
                mSignFaceListener.setSignInCutView(null);
            }
        });
    }

    @Override
    protected void onGrantedSuccess() {
        loginSign.setOnClickListener(v -> {
            if (isGranted()) {
                setInstallPermission();
                return;
            }
            initFinger();
        });
    }


}
