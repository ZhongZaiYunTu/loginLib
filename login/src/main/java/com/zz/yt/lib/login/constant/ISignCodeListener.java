package com.zz.yt.lib.login.constant;



/**
 * 手机号和验证码登录
 *
 * @author qf
 * @date 2019-12-12
 */
public interface ISignCodeListener extends ASignLit{

    /**
     * 获取验证码
     *
     * @param phone:手机号
     */
    void onPhoneCode(String phone);

    /**
     * 登录
     *
     * @param phone:手机号
     * @param code:验证码
     */
    void onPhoneSignIn(String phone, String code);

}
