package com.zz.yt.lib.login.view;

import android.content.Context;
import android.util.AttributeSet;

import androidx.annotation.Nullable;

import com.blankj.utilcode.util.ToastUtils;
import com.zz.yt.lib.login.R;
import com.zz.yt.lib.login.checker.AgreementPrivacyManager;
import com.zz.yt.lib.ui.privacy.IPrivacyListener;
import com.zz.yt.lib.ui.view.SelectImageView;
import com.zz.yt.lib.ui.view.base.BaseLinearLayout;

/**
 * 用户协议和隐私政策
 *
 * @author hf
 * @version 1.0
 */
public final class AgreementPrivacyView extends BaseLinearLayout {

    private final SelectImageView idImageSelect;

    /**
     * 设置跳转到用户协议和隐私政策
     */
    protected IPrivacyListener mIPrivacyListener = null;

    @Override
    protected int setLayout() {
        return R.layout.hai_view_agreement_privacy;
    }

    public final void setIPrivacyListener(IPrivacyListener listener) {
        this.mIPrivacyListener = listener;
    }

    public AgreementPrivacyView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        idImageSelect = findViewById(R.id.login_img_select);
        if (idImageSelect != null) {
            idImageSelect.setChecked(AgreementPrivacyManager.isAgrPrivacy());
            idImageSelect.setOnClickListener(v -> {
                idImageSelect.select();
                AgreementPrivacyManager.setAgrPrivacy(idImageSelect.isChecked());
            });
        }

        setOnClickListener(R.id.text_agreement, v -> {
            //用户协议
            if (mIPrivacyListener != null) {
                mIPrivacyListener.onClickAgreement();
            }
        });

        setOnClickListener(R.id.text_privacy, v -> {
            //隐私政策
            if (mIPrivacyListener != null) {
                mIPrivacyListener.onClickPrivacy();
            }
        });

    }

}
