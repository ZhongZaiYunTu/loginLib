package com.zz.yt.lib.login.checker;


import com.whf.android.jar.util.storage.LattePreference;


/**
 * 开通指纹信息。
 *
 * @author qf
 * @version 22.4.22
 */
public class FingerManager {
    public static final String DEFAULT_KEY_NAME = "default_key";

    /*** 开通指纹信息 */
    private enum Tag {
        FINGER_TAG
    }

    /***
     * @param open:开通指纹信息
     */
    public static void setFinger(boolean open) {
        LattePreference.setAppFlag(Tag.FINGER_TAG.name(), open);
    }

    /**
     * 是否开通指纹信息
     */
    public static boolean isFinger() {
        return LattePreference.getAppFlag(Tag.FINGER_TAG.name());
    }


}
