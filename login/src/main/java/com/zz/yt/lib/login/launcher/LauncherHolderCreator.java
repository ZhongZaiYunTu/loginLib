package com.zz.yt.lib.login.launcher;

import android.view.View;

import com.bigkoo.convenientbanner.holder.CBViewHolderCreator;
import com.bigkoo.convenientbanner.holder.Holder;
import com.zz.yt.lib.login.R;

/**
 * 滑动画册
 *
 * @author 傅令杰
 * @date 2017/4/22
 */

public class LauncherHolderCreator implements CBViewHolderCreator {

    @Override
    public Holder createHolder(View itemView) {
        return new LauncherHolder(itemView);
    }

    @Override
    public int getLayoutId() {
        return R.layout.hai_delegate_launcher_creator;
    }
}
