package com.zz.yt.lib.login.checker;


import com.whf.android.jar.util.storage.LattePreference;


/**
 * 一周内自动登录
 *
 * @author qf
 * @version 2022/4/22
 */
public class TravelManager {

    /**
     * 一周内自动登录
     */
    private enum Tag {
        TRAVEL_TAG
    }

    //region 一周内自动登录

    /***
     * @param state:一周内自动登录（true:用户； or false:游客）
     */
    public static void setRoleState(boolean state) {
        LattePreference.setAppFlag(Tag.TRAVEL_TAG.name(), state);
    }

    /**
     * 一周内自动登录
     */
    public static boolean isTravel() {
        return LattePreference.getAppFlag(Tag.TRAVEL_TAG.name());
    }

    //endregion 一周内自动登录

}
