package com.zz.yt.lib.login.delegate.debug;

import android.widget.TextView;

import androidx.annotation.NonNull;

import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.JsonUtils;
import com.whf.android.jar.constants.AppConstant;
import com.whf.android.jar.constants.HttpConstant;
import com.whf.android.jar.net.HttpCode;
import com.whf.android.jar.net.RestClient;
import com.whf.android.jar.util.log.LatteLogger;

import java.util.List;


/**
 * 切换服务
 *
 * @author qf
 * @version 1.0
 **/
public class LoginServerUtils extends BaseLoginUtils {

    @NonNull
    public static LoginServerUtils create(TextView textView, String code) {
        return new LoginServerUtils(textView, code);
    }

    protected LoginServerUtils(TextView textView, String code) {
        super(textView);
        RestClient.builder()
                .url(AppConstant.TEST_SERVICE + code)
                .success(response -> {
                    LatteLogger.json(response);
                    if (HttpCode.CODE_200 == JsonUtils.getInt(response, HttpConstant.CODE)) {
                        final String data = JsonUtils.getString(response, HttpConstant.DATA);
                        List<PickerEntry> mArray = GsonUtils.fromJson(data, GsonUtils.getListType(PickerEntry.class));
                        if (mArray != null) {
                            jsonArray.clear();
                            jsonArray.addAll(mArray);
                        }
                        setText(options);
                    }
                })
                .build()
                .get();
    }




}
