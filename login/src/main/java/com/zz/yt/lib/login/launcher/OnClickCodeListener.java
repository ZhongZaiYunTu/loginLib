package com.zz.yt.lib.login.launcher;

public interface OnClickCodeListener {

    /**
     * 登录
     *
     * @param phone:手机号
     */
    void onPhoneCode(String phone);

}
