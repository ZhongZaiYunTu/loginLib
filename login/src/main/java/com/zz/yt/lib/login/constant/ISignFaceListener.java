package com.zz.yt.lib.login.constant;

import android.widget.Button;



/**
 * 人脸登录
 *
 * @author qf
 * @date 2020-7-12
 */
public interface ISignFaceListener {

    /**
     * 设置登录背景图片
     *
     * @return 图片
     */
    Object setSignIn();

    /**
     * 登录
     *
     * @param phone:账号
     * @param bitmap:人脸的图片地址
     */
    void onSignFaceIn(String phone, String bitmap);

    /**
     * 登录按钮设置
     *
     * @param signInView:登录按钮
     */
    void setSignInView(Button signInView);

    /**
     * 登录切换按钮设置
     * null时点击按钮，不为空时，设置布局界面
     *
     * @param signInCutView:登录切换按钮
     */
    void setSignInCutView(Button signInCutView);

}
