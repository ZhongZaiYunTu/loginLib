package com.zz.yt.lib.login.checker;

import com.whf.android.jar.util.storage.LattePreference;

/**
 * 是否使用离线数据
 *
 * @author qf
 * @version 2020/4/23
 */
public class OfflineManager {

    private enum OfflineTag {
        /**
         * 保存是否离线
         */
        USER_OFFLINE
    }

    //region 显示滑动启动页

    /**
     * 是否使用离线数据
     *
     * @return 是否
     */
    public static boolean isOfflineLauncher() {
        return !LattePreference.getAppFlag(OfflineManager.OfflineTag.USER_OFFLINE.name());
    }

    /**
     * 是否使用离线数据
     */
    public static void setOfflineLauncher() {
        setOfflineLauncher(true);
    }

    /**
     * 是否使用离线数据
     *
     * @param flag:是否
     */
    public static void setOfflineLauncher(boolean flag) {
        LattePreference.setAppFlag(OfflineManager.OfflineTag.USER_OFFLINE.name(), flag);
    }

    //endregion
}