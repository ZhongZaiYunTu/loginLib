package com.zz.yt.lib.login.grade;

import android.content.Context;
import android.view.View;

import androidx.annotation.NonNull;

import com.blankj.utilcode.util.StringUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.whf.android.jar.app.Latte;
import com.whf.android.jar.net.RestClient;
import com.whf.android.jar.net.callback.IRequest;
import com.whf.android.jar.popup.listener.OnClickDecisionListener;
import com.whf.android.jar.util.log.LatteLogger;
import com.zz.yt.lib.login.R;
import com.zz.yt.lib.login.grade.bean.GradeBean;


/**
 * 升级版本
 *
 * @author qf
 * @version 22.3.12
 */
public class UpGradeUtils implements OnClickDecisionListener {

    private GradeBean mGradeBean = null;
    private final UpGradePopup mComplexPopup;
    private View.OnClickListener mClickListener = null;

    private UpGradeUtils(Context context) {
        this.mComplexPopup = UpGradePopup.create(context);
    }

    @NonNull
    public static UpGradeUtils create() {
        return new UpGradeUtils(Latte.getActivity());
    }

    public UpGradeUtils setDate(GradeBean gradeBean) {
        this.mGradeBean = gradeBean;
        return this;
    }

    public UpGradeUtils setOnClickListener(View.OnClickListener listener) {
        this.mClickListener = listener;
        return this;
    }

    public void show() {
        if (mGradeBean != null) {
            mComplexPopup
                    .setGradeBean(mGradeBean)
                    .setOnClickListener(this)
                    .popup();
        }
    }

    @Override
    public void onConfirm(Object decision) {
        if (mGradeBean != null) {
            LatteLogger.i("开始更新，下载地址: " + mGradeBean.getApkPath());
            RestClient.builder()
                    .url(mGradeBean.getApkPath())
                    .name(StringUtils.getString(R.string.apk_name))
                    .failure(this::dismiss)
                    .error((code, msg) -> {
                        dismiss();
                        LatteLogger.e(msg == null ? "下载失败，请稍后再试。" : msg);
                        ToastUtils.showShort(msg == null ? "下载失败，请稍后再试。" : msg);
                    })
                    .onRequest(new IRequest() {
                        @Override
                        public void onRequestStart() {

                        }

                        @Override
                        public void onRequestProgress(int progress) {

                        }

                        @Override
                        public void onRequestEnd() {
                            dismiss();
                            if (mClickListener != null) {
                                mClickListener.onClick(null);
                            }
                        }
                    })
                    .build()
                    .download();
        }

    }

    @Override
    public void onCancel() {
        if (mClickListener != null) {
            mClickListener.onClick(null);
        }
    }

    private void dismiss() {
        if (mComplexPopup != null) {
            mComplexPopup.dismiss();
        }
    }
}
