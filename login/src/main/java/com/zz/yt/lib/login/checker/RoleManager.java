package com.zz.yt.lib.login.checker;


import com.whf.android.jar.util.storage.LattePreference;


/**
 * 角色选择
 *
 * @author qf
 * @version 2022/4/22
 */
public class RoleManager {

    private enum RoleTag {
        /**
         * 角色字段
         */
        ROLE_TAG
    }

    //region 角色字段

    /***
     * @param state:角色字段（true:用户； or false:游客）
     */
    public static void setRoleState(boolean state) {
        LattePreference.setAppFlag(RoleTag.ROLE_TAG.name(), state);
    }

    /**
     * true:用户； or false:游客
     * 默认是游客
     */
    public static boolean isRole() {
        return LattePreference.getAppFlag(RoleTag.ROLE_TAG.name());
    }

    //endregion 角色字段

}
