package com.zz.yt.lib.login.delegate.login;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.blankj.utilcode.constant.PermissionConstants;
import com.blankj.utilcode.util.PermissionUtils;
import com.blankj.utilcode.util.ScreenUtils;
import com.whf.android.jar.app.Latte;
import com.whf.android.jar.util.log.LatteLogger;
import com.zz.yt.lib.annex.utils.HeadUtils;
import com.zz.yt.lib.login.R;
import com.zz.yt.lib.login.checker.AccountManager;
import com.zz.yt.lib.login.constant.ISignFaceListener;
import com.zz.yt.lib.login.delegate.login.base.BaseSigInDelegate;
import com.zz.yt.lib.login.face.FaceDistinguishPopup;
import com.zz.yt.lib.login.face.listener.OnClickFaceListener;

import org.jetbrains.annotations.NotNull;

import java.util.List;


/**
 * 人脸登录
 *
 * @author qf
 * @version 2020-1-03
 */
public class SignInFaceDelegate extends BaseSigInDelegate implements OnClickFaceListener {

    private Button loginSign = null;
    private ISignFaceListener mSignFaceListener = null;

    @Override
    public Object setLayout() {
        return R.layout.hai_delegate_sign_in_face;
    }

    @Override
    public void onAttach(@NonNull Context activity) {
        super.onAttach(activity);
        //这里是类型转换，而不是判断
        if (activity instanceof ISignFaceListener) {
            mSignFaceListener = (ISignFaceListener) activity;
        }
    }

    private void inBundle() {
        if (mSignFaceListener != null) {
            Object launcher = mSignFaceListener.setSignIn();
            if (launcher != null) {
                if (launcher instanceof Integer) {
                    getImageView(R.id.login_image_background)
                            .setImageResource((int) launcher);
                } else if (launcher instanceof View) {
                    View viewLauncher = (View) launcher;
                    RelativeLayout view = findViewById(R.id.login_layout_background);
                    ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.MATCH_PARENT);
                    viewLauncher.setLayoutParams(layoutParams);
                    view.addView(viewLauncher);
                }
            }
        }
        //登录状态为未登录
        AccountManager.setSignState(false);
    }

    @Override
    public void onBindView(@Nullable Bundle savedInstanceState, @NonNull View rootView) {
        inBundle();
        initView();
        inGradePermission();
    }

    private void initView() {
        //头像
        HeadUtils.preview(getImageView(R.id.login_image_portrait));
        //账号
        getTextView(R.id.login_text_phone).setText(getPhone());
        //登录按钮
        loginSign = getButton(R.id.login_btn_sign_in);
        loginSign.setEnabled(getPhone().length() > 1);
        if (mSignFaceListener != null) {
            mSignFaceListener.setSignInView(loginSign);
        }
        //切换登录方式
        final Button loginSignCut = getButton(R.id.login_btn_sign_in_cut);
        if (mSignFaceListener != null) {
            mSignFaceListener.setSignInCutView(loginSignCut);
        }
        loginSignCut.setOnClickListener(v -> {
            if (mSignFaceListener != null) {
                mSignFaceListener.setSignInCutView(null);
            }
        });
    }

    @Override
    protected void onGrantedSuccess() {
        loginSign.setOnClickListener(v -> {
            if (isGranted()) {
                setInstallPermission();
                return;
            }
            //摄像机权限检测
            inCameraPermission();
        });
    }

    /**
     * 摄像机权限检测
     */
    private void inCameraPermission() {
        PermissionUtils
                .permission(PermissionConstants.CAMERA, PermissionConstants.STORAGE)
                .rationale((activity, shouldRequest) -> shouldRequest.again(true))
                .callback(new PermissionUtils.FullCallback() {
                    @Override
                    public void onGranted(@NotNull List<String> permissionsGranted) {
                        LatteLogger.d("权限获取成功", permissionsGranted);
                        onFace();
                    }

                    @Override
                    public void onDenied(@NotNull List<String> permissionsDeniedForever, @NotNull List<String> permissionsDenied) {
                        LatteLogger.d("权限永久拒绝", permissionsDeniedForever);
                        LatteLogger.d("权限拒绝", permissionsDenied);
                        if (getProxyActivity() != null) {
                            getProxyActivity().finish();
                        }
                    }
                })
                .theme(ScreenUtils::setFullScreen)
                .request();
    }

    protected void onFace() {
        FaceDistinguishPopup.create(Latte.getActivity())
                .setOnClickListener(this)
                .start();
    }


    @Override
    public void onConfirm(String bitmap) {
        LatteLogger.i(bitmap);
        if (mSignFaceListener != null) {
            mSignFaceListener.onSignFaceIn(getPhone(), bitmap);
        }
        if (Latte.getDebug()) {
            HeadUtils.preview(getImageView(R.id.login_image_portrait),
                    bitmap,
                    R.mipmap.iv_default_head_image);
        }
    }

}