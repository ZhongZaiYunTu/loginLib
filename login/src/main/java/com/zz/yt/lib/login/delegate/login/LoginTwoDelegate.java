package com.zz.yt.lib.login.delegate.login;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.whf.android.jar.app.Latte;
import com.zz.yt.lib.login.R;
import com.zz.yt.lib.login.checker.AgreementPrivacyManager;
import com.zz.yt.lib.login.constant.ILoginListener;
import com.zz.yt.lib.login.constant.ISignUpListener;
import com.zz.yt.lib.login.delegate.login.base.BaseFingerDelegate;
import com.zz.yt.lib.ui.item.EditLayoutBar;
import com.zz.yt.lib.ui.view.SelectImageView;


/**
 * 登录
 *
 * @author qf
 * @version 2020-1-03
 */
public class LoginTwoDelegate extends BaseFingerDelegate {


    private EditLayoutBar idEditPassword;
    private Button loginSign;

    private ILoginListener mLoginListener = null;
    private ISignUpListener mSignUpListener = null;

    @Override
    public void onAttach(@NonNull Context activity) {
        super.onAttach(activity);
        //这里是类型转换，而不是判断
        if (activity instanceof ILoginListener) {
            mLoginListener = (ILoginListener) activity;
        }
        if (activity instanceof ISignUpListener) {
            mSignUpListener = (ISignUpListener) activity;
        }
    }

    @Override
    public Object setLayout() {
        return R.layout.hai_delegate_login_two;
    }

    private void inBundle() {
        if (mLoginListener != null) {
            Object launcher = mLoginListener.setSignIn();
            if (launcher != null) {
                if (launcher instanceof Integer) {
                    getImageView(R.id.login_image_background)
                            .setImageResource((int) launcher);
                } else if (launcher instanceof View) {
                    View viewLauncher = (View) launcher;
                    RelativeLayout view = findViewById(R.id.login_layout_background);
                    ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.MATCH_PARENT);
                    viewLauncher.setLayoutParams(layoutParams);
                    view.addView(viewLauncher);
                }
            }
        }
        //设置是否保存登录信息
        final SelectImageView idSelect = findViewById(R.id.login_img_acc_pass);
        setImgSelect(idSelect);


        //注册
        boolean isSignUp = false;
        if (mSignUpListener != null) {
            isSignUp = mSignUpListener.isSignUp();
            mSignUpListener.setSignUpView(mRootView, getTextView(R.id.login_text_sign_up));
        }
        getTextView(R.id.login_text_sign_up).setVisibility(isSignUp ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onBindView(@Nullable Bundle savedInstanceState, @NonNull View rootView) {
        inBundle();
        initView();
        inGradePermission();
    }

    private void initView() {
        agreementPrivacy();

        getEditTextBar(R.id.login_edit_account).setText(getAccount());
        idEditPassword = getEditTextBar(R.id.login_edit_password);
        inPassword(idEditPassword);

        loginSign = getButton(R.id.login_btn_sign_in);
        loginSign.setEnabled(idEditPassword.toString().length() > 1);

        if (mLoginListener != null) {
            mLoginListener.setSignInView(loginSign);
            //测试服务器
            TextView textDebug = getTextView(R.id.text_debug);
            if (textDebug != null) {
                textDebug.setVisibility(Latte.isSwitchingServices() ? View.VISIBLE : View.GONE);
                mLoginListener.setServiceSport(textDebug);
            }
        }
    }


    @Override
    protected void onGrantedSuccess() {
        loginSign.setOnClickListener(view -> {
            if (isGranted()) {
                setInstallPermission();
                return;
            }
            if (AgreementPrivacyManager.is()) {
                return;
            }
            if (mLoginListener != null) {
                setSelect(true);
                String account = getEditTextBar(R.id.login_edit_account).toString();
                String password = getEditTextBar(R.id.login_edit_password).toString();

                if (isAccount(account) && isPassword(password)) {
                    mLoginListener.onSignIn(account, password);
                }
            }
        });

    }


    @Override
    public void afterTextChanged(Editable editable) {
        if (idEditPassword != null && loginSign != null) {
            loginSign.setEnabled(idEditPassword.toString().length() > 1);
        }
    }

}