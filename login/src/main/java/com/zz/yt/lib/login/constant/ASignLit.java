package com.zz.yt.lib.login.constant;

import android.widget.Button;
import android.widget.TextView;

interface ASignLit {

    /**
     * 设置登录背景图片
     */
    Object setSignIn();

    /**
     * 是否显示服务地址选择
     */
    void setServiceSport(TextView textDebug);

    /**
     * @param signInView:登录按钮设置
     */
    void setSignInView(Button signInView);
}
