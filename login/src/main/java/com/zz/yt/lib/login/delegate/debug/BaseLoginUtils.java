package com.zz.yt.lib.login.delegate.debug;

import android.view.View;
import android.widget.TextView;

import com.blankj.utilcode.util.ObjectUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.whf.android.jar.app.ConfigKeys;
import com.whf.android.jar.app.Latte;
import com.whf.android.jar.net.RestClient;
import com.whf.android.jar.util.log.LatteLogger;
import com.whf.android.jar.util.storage.LattePreference;
import com.zz.yt.lib.ui.popup.utils.picker.PickerPopupUtils;

import java.util.ArrayList;
import java.util.List;


/**
 * 切换服务
 *
 * @author qf
 * @author 中再云图技术有限公司
 * @version 1.0.24
 **/
abstract class BaseLoginUtils implements View.OnClickListener {

    final static String SERVER = "server";

    protected final int options;
    protected final TextView mTextView;
    protected final List<PickerEntry> jsonArray = new ArrayList<>();

    /**
     * @param textView:切换服务
     */
    protected BaseLoginUtils(TextView textView) {
        this.mTextView = textView;
        this.options = Math.max(LattePreference.getAppSign(SERVER), 0);
        if (mTextView != null) {
            mTextView.setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View v) {
        if (jsonArray.size() == 0) {
            ToastUtils.showShort("未查询到数据，请稍后再试。");
            return;
        }
        PickerPopupUtils.showPickerData(Latte.getActivity(), "切换服务", jsonArray, options,
                (index, obj) -> setText(index));
    }

    /**
     * @param options:设置选中
     */
    protected void setText(int options) {
        if (jsonArray.size() > options) {
            LattePreference.setAppSign(SERVER, options);
            final PickerEntry pickerEntry = jsonArray.get(options);
            if (pickerEntry == null || ObjectUtils.isEmpty(pickerEntry.getComments())) {
                return;
            }
            setText(pickerEntry.getComments());
            if (mTextView != null) {
                mTextView.setText(pickerEntry.getPickerViewText());
            }
        }
    }


    /**
     * @param apiHost:设置服务地址
     */
    protected void setText(String apiHost) {
        LattePreference.addCustomAppProfile(ConfigKeys.API_HOST.name(), apiHost);
        Latte.getConfigurator().withApiHost(apiHost);
        RestClient.builder()
                .url("/bai/du/com/")
                .success(LatteLogger::json)
                .build()
                .get();
    }

}
