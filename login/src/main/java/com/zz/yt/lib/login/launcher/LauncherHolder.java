package com.zz.yt.lib.login.launcher;

import android.view.View;
import android.widget.ImageView;

import com.bigkoo.convenientbanner.holder.Holder;
import com.squareup.picasso.Picasso;
import com.zz.yt.lib.login.R;

/**
 * 滑动相册的展示界面
 *
 * @author 傅令杰
 * @date 2017/4/22
 */

public class LauncherHolder extends Holder<Object> {

    private ImageView mImageView;

    LauncherHolder(View itemView) {
        super(itemView);
        mImageView = itemView.findViewById(R.id.login_image_background);
    }

    @Override
    protected void initView(View itemView) {

    }

    @Override
    public void updateUI(Object data) {
        if(mImageView != null) {
            if (data instanceof Integer) {
                mImageView.setBackgroundResource((Integer) data);
            } else if (data instanceof String) {
                Picasso.get()
                        .load((String) data)
                        .into(mImageView);

            }
        }
    }
}
