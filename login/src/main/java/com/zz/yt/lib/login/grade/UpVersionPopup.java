package com.zz.yt.lib.login.grade;

import android.content.Context;
import android.view.View;

import androidx.annotation.NonNull;

import com.liys.view.LineProView;
import com.whf.android.jar.popup.LattePopupCenter;
import com.zz.yt.lib.login.R;
import com.zz.yt.lib.login.grade.bean.GradeBean;

/**
 * 升级
 *
 * @author qf
 * @version 22.3.12
 */
class UpVersionPopup extends LattePopupCenter implements View.OnClickListener {

    private final static int MAX = 100;
    private GradeBean mGradeBean = null;

    @NonNull
    static UpVersionPopup create(Context context) {
        return new UpVersionPopup(context);
    }

    private UpVersionPopup(Context context) {
        super(context);
    }

    @Override
    protected int setLayout() {
        return R.layout.hai_popup_up_version;
    }

    UpVersionPopup setGradeBean(GradeBean gradeBean) {
        this.mGradeBean = gradeBean;
        return this;
    }

    @Override
    protected void initViews() {
        setOnClickListener(R.id.login_cancel, v -> dismiss());
        setOnClickListener(R.id.login_confirm, this);
        if (mGradeBean != null) {
            setText(R.id.id_text_versions, mGradeBean.getVersionName());
        }
    }

    @Override
    public void onClick(View v) {
        if (mClickDecisionListener != null) {
            mClickDecisionListener.onConfirm(null);
        }
    }

    /**
     * @param progress:开始下载
     */
    void setProgressBar(int progress) {
        setShow(R.id.layout_versions, progress <= 0);
        setShow(R.id.layout_download, progress > 0);
        final LineProView proView = findViewById(R.id.id_line_pro_view);
        if (proView != null && progress > 0 && progress <= MAX) {
            proView.setProgress(progress);
            if (progress == MAX) {
                dismiss();
            }
        }
    }
}
