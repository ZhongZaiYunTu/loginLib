package com.zz.yt.lib.login.checker;

import com.whf.android.jar.app.Latte;
import com.whf.android.jar.util.storage.LattePreference;

/**
 * 切换服务管理
 *
 * @author 傅令杰
 * @version 1.2.2
 */
public class CloudManager {


    private enum Tag {
        /*** 地址服务*/
        HOST_API,

        /*** 地址服务*/
        WEB_API,

        /***二级域名 */
        FIELD,
    }

    /**
     * 初始化切换服务。
     */
    public static void initApp() {
        Latte.getConfigurator()
                .withApiHost(apiHost())
                .withApiField(apiField())
                .withWebHost(apiWeb())
                .withSwitchingServices(true);
    }

    //region 后台地址服务

    /**
     * @return 后台地址服务
     */
    private static String apiHost() {
        return apiHost("https://www.baidu.com");
    }

    /**
     * @return 后台地址服务
     */
    public static String apiHost(String value) {
        return LattePreference.getCustomAppProfile(Tag.HOST_API.name(), value);
    }


    /**
     * @param value:后台地址服务
     */
    public static void setApiHost(String value) {
        LattePreference.addCustomAppProfile(Tag.HOST_API.name(), value);
        Latte.getConfigurator().withApiHost(value);
    }

    //endregion


    //region 前台地址服务

    /**
     * @return 前台地址服务
     */
    private static String apiWeb() {
        return apiWeb("https://www.baidu.com");
    }


    /**
     * @return 前台地址服务
     */
    public static String apiWeb(String value) {
        return LattePreference.getCustomAppProfile(Tag.WEB_API.name(), value);
    }


    /**
     * @param value:前台地址服务
     */
    public static void setApiWeb(String value) {
        LattePreference.addCustomAppProfile(Tag.WEB_API.name(), value);
        Latte.getConfigurator().withWebHost(value);
    }

    //endregion


    //region 二级域名

    /**
     * @return 二级域名
     */
    private static String apiField() {
        return apiField("");
    }


    /**
     * @return 二级域名
     */
    public static String apiField(String value) {
        return LattePreference.getCustomAppProfile(Tag.FIELD.name(), value);
    }


    /**
     * @param value:二级域名
     */
    public static void setApiField(String value) {
        LattePreference.addCustomAppProfile(Tag.FIELD.name(), value);
        Latte.getConfigurator().withApiField(value);
    }

    //endregion
}
