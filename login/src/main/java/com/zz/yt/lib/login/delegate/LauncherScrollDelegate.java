package com.zz.yt.lib.login.delegate;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bigkoo.convenientbanner.ConvenientBanner;
import com.bigkoo.convenientbanner.listener.OnPageChangeListener;
import com.whf.android.jar.base.delegate.BaseDelegate;
import com.zz.yt.lib.login.R;
import com.zz.yt.lib.login.checker.AccountManager;
import com.zz.yt.lib.login.checker.IUserChecker;
import com.zz.yt.lib.login.launcher.ILauncherListener;
import com.zz.yt.lib.login.launcher.LauncherHolderCreator;
import com.zz.yt.lib.login.launcher.OnLauncherFinishTag;
import com.zz.yt.lib.login.launcher.privacy.ScrollLauncher;

import java.util.ArrayList;

/**
 * 滑动引导页
 *
 * @author qf
 * @version 2020-03-12
 */
public class LauncherScrollDelegate extends BaseDelegate implements OnPageChangeListener, View.OnClickListener {

    private Button mBenExperience = null;
    private ConvenientBanner<Object> mConvenientBanner = null;

    private ArrayList<Object> integers = new ArrayList<>();
    private ILauncherListener mLauncherListener = null;

    @NonNull
    public static LauncherScrollDelegate create() {
        Bundle args = new Bundle();

        final LauncherScrollDelegate fragment = new LauncherScrollDelegate();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof ILauncherListener) {
            mLauncherListener = (ILauncherListener) context;
        }
    }

    @Override
    protected boolean setMourning() {
        return true;
    }

    @Override
    public Object setLayout() {
        return R.layout.hai_delegate_launcher_scroll;
    }

    private void initView(@NonNull View rootView) {
        mConvenientBanner = rootView.findViewById(R.id.login_convenient_banner);
        mBenExperience = rootView.findViewById(R.id.login_experience);
        mBenExperience.setOnClickListener(this);
    }

    private void initBundle() {
        if (mLauncherListener != null) {
            integers = mLauncherListener.setLauncherScroll();
        }
    }

    @Override
    public void onBindView(@Nullable Bundle savedInstanceState, @NonNull View rootView) {
        initView(rootView);
        initBundle();
        initBanner();
    }

    private void initBanner() {
        if (integers != null && integers.size() > 0) {
            mConvenientBanner
                    .setPages(new LauncherHolderCreator(), integers)
                    .setPageIndicator(new int[]{R.drawable.login_ic_dot_normal, R.drawable.login_ic_dot_focus})
                    .setPageIndicatorAlign(ConvenientBanner.PageIndicatorAlign.CENTER_HORIZONTAL)
                    .setOnPageChangeListener(this)
                    .setPointViewVisible(true)
                    .setCanLoop(false);
        }
    }

    @Override
    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {

    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

    }

    @Override
    public void onPageSelected(int index) {
        //如果点击的是最后一个
        if (index == integers.size() - 1) {
            mBenExperience.setVisibility(View.VISIBLE);
            mConvenientBanner.setPointViewVisible(false);
        } else {
            mBenExperience.setVisibility(View.GONE);
            mConvenientBanner.setPointViewVisible(true);
        }
    }

    @Override
    public void onClick(View v) {
        ScrollLauncher.setScrollLauncher();
        //检查用户是否已经登录
        AccountManager.checkAccount(new IUserChecker() {
            @Override
            public void onSignIn() {
                if (mLauncherListener != null) {
                    mLauncherListener.onLauncherFinish(OnLauncherFinishTag.SIGNED);
                }
            }

            @Override
            public void onNotSignIn() {
                if (mLauncherListener != null) {
                    mLauncherListener.onLauncherFinish(OnLauncherFinishTag.NOT_SIGNED);
                }
            }
        });
    }

}
