package com.zz.yt.lib.login.checker;


import com.blankj.utilcode.util.ToastUtils;
import com.whf.android.jar.util.storage.LattePreference;

/**
 * 是否勾选隐私政策和用户协议
 *
 * @author qf
 * @version 2023/4/22
 */
public class AgreementPrivacyManager {

    private enum Tag {
        /**
         * 是否勾选隐私政策和用户协议
         */
        AGREEMENT_PRIVACY_IS

    }

    //region 登录状态

    /**
     * @param state:是否勾选隐私政策和用户协议
     */
    public static void setAgrPrivacy(boolean state) {
        LattePreference.setAppFlag(Tag.AGREEMENT_PRIVACY_IS.name(), state);
    }

    public static boolean isAgrPrivacy() {
        return LattePreference.getAppFlag(Tag.AGREEMENT_PRIVACY_IS.name());
    }

    public static boolean is() {
        if (!isAgrPrivacy()) {
            String hint = "请阅读并同意用户协议和隐私政策";
            ToastUtils.showShort(hint);
            return true;
        }
        return false;
    }
    //endregion


}
