package com.zz.yt.lib.login.launcher.privacy;

import com.whf.android.jar.util.storage.LattePreference;

/**
 * 判断是否显示滑动启动页
 *
 * @author 傅令杰
 * @date 2017/4/22
 */

public class ScrollLauncher {


    private enum ScrollLauncherTag {
        /**
         * 保存否显示滑动启动页
         */
        HAS_FIRST_LAUNCHER_APP
    }

    //region 显示滑动启动页

    /**
     * 判断是否显示滑动启动页
     *
     * @return 是否
     */
    public static boolean isScrollLauncher() {
        return !LattePreference.getAppFlag(ScrollLauncherTag.HAS_FIRST_LAUNCHER_APP.name());
    }

    /**
     * 设置不显示滑动启动页
     */
    public static void setScrollLauncher() {
        setScrollLauncher(true);
    }

    /**
     * 设置是否显示滑动启动页
     *
     * @param flag:是否
     */
    public static void setScrollLauncher(boolean flag) {
        LattePreference.setAppFlag(ScrollLauncherTag.HAS_FIRST_LAUNCHER_APP.name(), flag);
    }

    //endregion
}
