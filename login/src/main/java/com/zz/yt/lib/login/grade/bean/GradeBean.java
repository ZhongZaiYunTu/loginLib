package com.zz.yt.lib.login.grade.bean;

import java.io.Serializable;
import java.util.List;

/**
 * 升级
 *
 * @author qf
 * @date 2020-04-20
 */
public class GradeBean implements Serializable {

    /**
     * 下载地址
     */

    private String apkPath;
    private String appDesc;
    private String compelUpdate;
    /**
     * 关系内容
     */
    private String content;
    /**
     * 图标
     */
    private String icon;
    /**
     * 是否更新
     */
    private boolean needUpdate;
    /**
     * 版本号
     */
    private double versionCode;
    /**
     * 版本名称
     */
    private String versionName;
    /**
     * 引导图
     */
    private List<String> appPics;

    /**
     * 其他
     */
    private Object value;

    public GradeBean() {

    }

    public GradeBean(String content, String versionName) {
        this.content = content;
        this.versionName = versionName;
    }

    public String getApkPath() {
        return apkPath;
    }

    public void setApkPath(String apkPath) {
        this.apkPath = apkPath;
    }

    public String getAppDesc() {
        return appDesc;
    }

    public void setAppDesc(String appDesc) {
        this.appDesc = appDesc;
    }

    public String getCompelUpdate() {
        return compelUpdate;
    }

    public void setCompelUpdate(String compelUpdate) {
        this.compelUpdate = compelUpdate;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public boolean isNeedUpdate() {
        return needUpdate;
    }

    public void setNeedUpdate(boolean needUpdate) {
        this.needUpdate = needUpdate;
    }

    public double getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(double versionCode) {
        this.versionCode = versionCode;
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    public List<String> getAppPics() {
        return appPics;
    }

    public void setAppPics(List<String> appPics) {
        this.appPics = appPics;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }
}