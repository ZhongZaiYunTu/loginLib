package com.zz.yt.lib.login.constant;



/**
 * 登录
 *
 * @author qf
 * @date 2019-12-12
 */
public interface ISignListener extends ASignLit{

    /**
     * 登录
     *
     * @param phone:手机号
     * @param password:密码
     */
    void onSignIn(String phone, String password);

}
