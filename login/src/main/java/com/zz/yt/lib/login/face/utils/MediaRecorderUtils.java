package com.zz.yt.lib.login.face.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.media.MediaRecorder;

import com.blankj.utilcode.util.ThreadUtils;

import java.io.File;
import java.util.concurrent.TimeUnit;

/**
 * @author qf
 * @author 中再云图技术有限公司
 * @version 1.0
 * @date 2020/10/26
 **/
public class MediaRecorderUtils {


    private int timer = 3;
    private boolean isRecording;
    private String videoPath;
    private MediaRecorder mRecorder;
    private Callback mCallback = null;

    public MediaRecorderUtils(Context context) {
        try {
            videoPath = getVideoFilePath(context);
            // 创建保存录制视频的视频文件
            File videoFile = new File(videoPath);
            // 创建MediaPlayer对象
            mRecorder = new MediaRecorder();
            mRecorder.reset();
            // 设置从麦克风采集声音(或来自录像机的声音AudioSource.CAMCORDER)
            mRecorder.setAudioSource(MediaRecorder
                    .AudioSource.MIC);
            // 设置从摄像头采集图像
            mRecorder.setVideoSource(MediaRecorder
                    .VideoSource.CAMERA);
            // 设置视频文件的输出格式
            // 必须在设置声音编码格式、图像编码格式之前设置
            mRecorder.setOutputFormat(MediaRecorder
                    .OutputFormat.THREE_GPP);
            // 设置声音编码的格式
            mRecorder.setAudioEncoder(MediaRecorder
                    .AudioEncoder.AMR_NB);
            // 设置图像编码的格式
            mRecorder.setVideoEncoder(MediaRecorder
                    .VideoEncoder.H264);
            mRecorder.setVideoSize(1280, 720);
            // 每秒 4帧
            mRecorder.setVideoFrameRate(20);
            //
            mRecorder.setOutputFile(videoFile.getAbsolutePath());
            mRecorder.prepare();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public MediaRecorderUtils setTimer(int timer) {
        this.timer = timer;
        return this;
    }

    public void setCallback(Callback callback) {
        this.mCallback = callback;
    }

    /**
     * 开始录制
     */
    public void start() {
        if (!isRecording) {
            startTime();
            mRecorder.start();
            isRecording = true;
        }
    }

    public void stop() {
        if (isRecording) {
            // 停止录制
            mRecorder.stop();
            // 释放资源
            mRecorder.release();
            mRecorder = null;
            if (mCallback != null) {
                mCallback.onVideoFile(videoPath);
            }
        }
    }

    private void startTime() {
        timer = 2;
        ThreadUtils.executeByFixedAtFixRate(1, new ThreadUtils.SimpleTask<Integer>() {

            @SuppressLint("SimpleDateFormat")
            @Override
            public Integer doInBackground() {
                timer--;
                return timer;
            }

            @SuppressLint("SetTextI18n")
            @Override
            public void onSuccess(Integer result) {
                if (timer <= 1) {
                    stop();
                }
            }
        }, 1000, TimeUnit.MILLISECONDS);
    }

    private String getVideoFilePath(Context context) {
        final File dir = context.getExternalFilesDir(null);
        return (dir == null ? "" : (dir.getAbsolutePath() + "/"))
                + System.currentTimeMillis() + ".mp4";
    }

    public interface Callback {

        /**
         * 检测到面部
         *
         * @param videoFile:
         */
        void onVideoFile(String videoFile);
    }
}

