package com.zz.yt.lib.login.badge;

/**
 * 厂商
 */
interface BadgeKeys {

    /**
     * 应用频道Id唯一值， 长度若太长可能会被截断
     */
    String CHANNEL_ID = "luo_zou_li051201";

    /**
     * 最长40个字符，太长会被截断
     */
    String CHANNEL_NAME = "badge_server";

    /**
     * 荣耀
     */
    String HONOR = "HONOR";

    /**
     * 华为
     */
    String HUO_WEI = "HUAWEI";
    /**
     * 华为
     */
    String HUO_WEI_1 = "Huawei";

    /**
     * 小米
     */
    String XI_MI = "Xiaomi";

    /**
     * 小米
     */
    String RED_MI = "redmi";

    /**
     * vivi
     */
    String VI_VO = "vivo";

    /**
     * oppo
     */
    String OP_PO = "OPPO";

    /**
     * IQOO
     */
    String IQOO = " iQOO";

    /**
     * 真我
     */
    String REAL_ME = "Realme";

    /**
     * 一加
     */
    String ONE_PLUS = "OnePlus";

    /**
     * 三星
     */
    String SAM_SUNG = "Samsung";

    /**
     * 三星
     */
    String SAM_SUNG_1 = "sumsang";

    /**
     * 魅族
     */
    String MEI_ZU = "meizu";

    /**
     * 魅族
     */
    String MEI_ZU_1 = "MEIZU";

    /**
     * 传音
     */
    String INFINIX = "infinix";

    /**
     * 锤子
     */
    String SMARTISAN = "smartisan";

    /**
     * 华硕
     */
    String ASUS = "asus";

    /**
     * 联想
     */
    String LENOVO = "lenovo";

    /**
     * 微软
     */
    String HTC = "htc";

    /**
     * 努比亚
     */
    String NUBIA = "nubia";

    /**
     * 索力
     */
    String SONY = "sony";



}
