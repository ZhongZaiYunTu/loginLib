package com.zz.yt.lib.login.launcher;

/**
 * 检查用户是否已经登录
 *
 * @author 傅令杰
 * @version 27.4.22
 */
public enum OnLauncherFinishTag {
    /**
     * 用户已经登录
     */
    SIGNED,
    /**
     * 用户未登录
     */
    NOT_SIGNED
}
