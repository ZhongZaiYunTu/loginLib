package com.zz.yt.lib.login.utils;

import com.blankj.utilcode.util.LogUtils;

import java.lang.reflect.Method;

/**
 * 鸿蒙系统
 *
 * @author qf
 * @version 1.0
 */
public class HarmonyUtils {


    private static final String HARMONY_OS = "harmony";

    /**
     * check the system is harmony os
     * <p>
     * 判断是否是鸿蒙系统
     *
     * @return true if it is harmony os
     */
    public static boolean isHarmonyOS() {
        try {
            Class<?> clz = Class.forName("com.huawei.system.BuildEx");
            Method method = clz.getMethod("getOsBrand");
            return HARMONY_OS.equals(method.invoke(clz));
        } catch (ClassNotFoundException e) {
            LogUtils.d("occured ClassNotFoundException");
        } catch (NoSuchMethodException e) {
            LogUtils.d("occured NoSuchMethodException");
        } catch (Exception e) {
            LogUtils.d("occured other problem");
        }
        return false;
    }
}
