package com.zz.yt.lib.login.constant;


/**
 * 登录
 *
 * @author qf
 * @version 2019-12-12
 */
public interface ILoginListener extends ASignLit {


    /**
     * 登录
     *
     * @param account:账号
     * @param password:密码
     */
    void onSignIn(String account, String password);

}
