package com.zz.yt.lib.login.face.listener;


/**
 * 获取的人脸后回传的事件
 *
 * @author qf
 * @version 1.0
 **/
public interface OnClickFaceListener {

    /**
     * 获取的人脸图片路径
     *
     * @param bitmap:
     */
    void onConfirm(String bitmap);

}
