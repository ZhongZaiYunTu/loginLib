package com.zz.yt.lib.login;


import com.blankj.utilcode.util.ObjectUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.whf.android.jar.app.ConfigKeys;
import com.whf.android.jar.app.Latte;
import com.whf.android.jar.base.delegate.BaseDelegate;
import com.whf.android.jar.base.delegate.IPageLoadListener;
import com.whf.android.jar.net.HttpCode;
import com.whf.android.jar.util.storage.LattePreference;
import com.zz.yt.lib.login.checker.AccountManager;
import com.zz.yt.lib.login.delegate.LauncherDelegate;
import com.zz.yt.lib.login.launcher.ILauncherListener;
import com.zz.yt.lib.login.launcher.OnLauncherFinishTag;
import com.zz.yt.lib.login.utils.GradeUtils;
import com.zz.yt.lib.ui.base.BaseExampleActivity;
import com.zz.yt.lib.ui.delegate.edition.listener.IEditionRecord;

import org.jetbrains.annotations.NotNull;


import me.yokeyword.fragmentation.anim.DefaultHorizontalAnimator;
import me.yokeyword.fragmentation.anim.FragmentAnimator;

/**
 * 登录的界面
 *
 * @author qf
 * @version 2.5.2
 */
public abstract class BaseLoginActivity extends BaseExampleActivity implements ILauncherListener,
        IEditionRecord, IPageLoadListener {

    @Override
    public BaseDelegate setRootDelegate() {
        boolean outSignIn = true;
        if (getIntent() != null) {
            outSignIn = getIntent().getBooleanExtra(STATE_OUT, true);
        }
        if (!outSignIn) {
            AccountManager.setSignState(false);
        }
        return outSignIn ? LauncherDelegate.create() : outSignInDelegate();
    }

    @Override
    public void onLauncherFinish(@NotNull OnLauncherFinishTag tag) {
        appInit();
        switch (tag) {
            case SIGNED:
                final String apiHost = LattePreference.getCustomAppProfile(ConfigKeys.API_HOST.name());
                if (Latte.isSwitchingServices() && !ObjectUtils.isEmpty(apiHost)) {
                    Latte.getConfigurator().withApiHost(apiHost);
                }
                startMain();
                break;
            case NOT_SIGNED:
                getSupportDelegate().start(outSignInDelegate());
                break;
            default:
                break;
        }
    }

    /*** 服务初始化 如 QbSdkUtils.init()；*/
    protected abstract void appInit();

    @Override
    public FragmentAnimator onCreateFragmentAnimator() {
        return new DefaultHorizontalAnimator();
    }

    /**
     * 主界面
     */
    protected abstract void startMain();


    @Override
    public void onError(int code, String msg) {
        hideLoading();
        if (HttpCode.CODE_404 == code) {
            ToastUtils.showShort("服务器找不到请求的接口");
        } else if (HttpCode.CODE_500 == code) {
            ToastUtils.showShort("服务器遇到错误，无法完成请求");
        } else if (ObjectUtils.isEmpty(msg)) {
            ToastUtils.showShort("服务器异常，请联系管理员");
        } else {
            ToastUtils.showShort(ObjectUtils.isEmpty(msg) ? "登录失败，请稍后再试。" : msg);
        }
    }

    @Override
    public void onEditionRecord() {
        GradeUtils.upGradeNet();
    }
}