package com.zz.yt.lib.login.checker;


import com.whf.android.jar.util.storage.LattePreference;

/**
 * 登录状态
 *
 * @author qf
 * @version 2022/4/22
 */
public class AccountManager {

    private enum SignTag {
        /**
         * 登录成功字段
         */
        SIGN_TAG
    }

    //region 登录状态

    /**
     * 保存用户登录状态，登录后调用
     *
     * @param state:
     */
    public static void setSignState(boolean state) {
        LattePreference.setAppFlag(SignTag.SIGN_TAG.name(), state);
    }

    public static boolean isSignIn() {
        return LattePreference.getAppFlag(SignTag.SIGN_TAG.name());
    }

    public static void checkAccount(IUserChecker checker) {
        if (isSignIn()) {
            checker.onSignIn();
        } else {
            checker.onNotSignIn();
        }
    }
    //endregion


}
