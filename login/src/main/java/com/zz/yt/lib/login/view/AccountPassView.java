package com.zz.yt.lib.login.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.Nullable;

import com.blankj.utilcode.util.ObjectUtils;
import com.whf.android.jar.app.Latte;
import com.whf.android.jar.dao.UserDao;
import com.whf.android.jar.net.HttpCode;
import com.zz.yt.lib.login.R;
import com.zz.yt.lib.ui.listener.OnClickBoolListener;
import com.zz.yt.lib.ui.view.base.BaseLinearLayout;


/**
 * 账号及密码自定义控件
 *
 * @author qf
 * @version 1.0
 */
public class AccountPassView extends BaseLinearLayout implements TextWatcher, View.OnTouchListener {

    private final EditText idEditPassword;

    private OnClickBoolListener mClickBoolListener = null;

    protected int setLayout() {
        return R.layout.hai_view_account_pass;
    }

    @SuppressLint("ClickableViewAccessibility")
    public AccountPassView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.AccountPassView);
        if (attributes != null) {

            boolean editable = attributes.getBoolean(R.styleable.AccountPassView_android_enabled, true);
            setXianShow(editable);

            String hint = attributes.getString(R.styleable.AccountPassView_android_hint);
            setAccountHint(hint);

            //回收属性
            attributes.recycle();
        }
        getEditText(R.id.login_edit_account).setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                findViewById(R.id.login_view_account).setBackgroundResource(R.color.colorAccent);
            } else {
                findViewById(R.id.login_view_account).setBackgroundResource(R.color.dim_grey);
            }
        });

        idEditPassword = getEditText(R.id.login_edit_password);
        idEditPassword.addTextChangedListener(this);
        idEditPassword.setOnTouchListener(this);
        idEditPassword.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                findViewById(R.id.login_view_password).setBackgroundResource(R.color.colorAccent);
            } else {
                findViewById(R.id.login_view_password).setBackgroundResource(R.color.dim_grey);
            }
        });
    }

    /**
     * @param hint:账号输入提示
     */
    public void setAccountHint(String hint) {
        if (ObjectUtils.isNotEmpty(hint)) {
            setTextHint(R.id.login_edit_account, hint);
        }
    }

    /**
     * @param show:是否显示输入框的下横线。
     */
    public void setXianShow(boolean show) {
        setShow(R.id.login_view_account, show);
        setShow(R.id.login_view_password, show);
    }

    /**
     * 回填密码
     */
    public void init() {
        init(true);
    }

    /**
     * @param is:是否回填密码
     */
    public void init(boolean is) {
        setText(R.id.login_edit_account, UserDao.getUserUser());
        if (is) {
            setText(R.id.login_edit_password, UserDao.getUserPass());
        }
        Latte.getHandler().postDelayed(() -> {
            if (mClickBoolListener != null) {
                mClickBoolListener.onClick(getPassword().length() > 1);
            }
        }, HttpCode.CODE_404);
    }

    /**
     * EditText的drawableRight属性设置点击事件
     */
    @SuppressLint("ClickableViewAccessibility")
    public boolean onTouch(View v, MotionEvent event) {
        // 得到一个长度为4的数组，分别表示左上右下四张图片(0.1.2.3)
        Drawable drawable = getDrawables(2);
        //如果右边没有图片，不再处理
        if (drawable == null)
            return false;
        //如果不是按下事件，不再处理
        if (event.getAction() != MotionEvent.ACTION_UP)
            return false;
        if (event.getX() > idEditPassword.getWidth()
                - idEditPassword.getPaddingRight()
                - drawable.getIntrinsicWidth()) {
            inPassword();
        }
        return false;
    }

    public Drawable getDrawables(int i) {
        if (idEditPassword != null) {
            return idEditPassword.getCompoundDrawables()[i];
        }
        return null;
    }


    /**
     * 密码显示隐藏
     */
    protected void inPassword() {
        if (idEditPassword.isSelected()) {
            idEditPassword.setSelected(false);
            setRightImage(idEditPassword, R.drawable.login_ic_visibility_off);
            idEditPassword.setInputType(InputType.TYPE_CLASS_TEXT
                    | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        } else {
            idEditPassword.setSelected(true);
            setRightImage(idEditPassword, R.drawable.login_ic_visibility_on);
            idEditPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
        }
        idEditPassword.setSelection(idEditPassword.getText().length());
    }


    public void setOnClickBoolListener(OnClickBoolListener listener) {
        this.mClickBoolListener = listener;
    }

    public String getAccount() {
        return getTextStr(R.id.login_edit_account);
    }

    public String getPassword() {
        return getTextStr(R.id.login_edit_password);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (mClickBoolListener != null) {
            mClickBoolListener.onClick(getPassword().length() > 1);
        }
    }
}
