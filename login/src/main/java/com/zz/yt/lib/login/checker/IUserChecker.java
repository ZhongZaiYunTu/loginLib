package com.zz.yt.lib.login.checker;

/**
 * 请求接口
 *
 * @author 傅令杰
 * @date 2017/4/22
 */

public interface IUserChecker {

    /**
     * 登录
     */
    void onSignIn();

    /**
     * 退出
     */
    void onNotSignIn();
}
