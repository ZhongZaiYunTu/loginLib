package com.zz.yt.lib.login.utils;


import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.whf.android.jar.app.Latte;
import com.whf.android.jar.net.RestClient;
import com.zz.yt.lib.login.grade.UpDateUtils;
import com.zz.yt.lib.login.grade.bean.GradeBean;


/**
 * 更新apk
 *
 * @author qf
 * @version 1.0
 */
public final class GradeUtils {


    /*** 更新内容 */
    private static GradeBean mGradeBean = null;

    /*** 查询最新的版本 */
    public static void upGradeNet() {
        RestClient.builder()
                .url(Latte.getCheckVersionServer())
                .params("appId", Latte.getAppId())
                .params("versionCode", Latte.getVersionCode())
                .analysis(true)
                .success(response -> {
                    mGradeBean = GsonUtils.fromJson(response, GradeBean.class);
                    final boolean needUpdate = mGradeBean.isNeedUpdate();
                    if (needUpdate) {
                        showPopup();
                    } else {
                        ToastUtils.showShort("目前版本是最新");
                    }
                })
                .build()
                .get();
    }


    /**
     * 成功获取安装权限，弹出需要更新和更新内容提示框
     */
    private static void showPopup() {
        UpDateUtils.create()
                .setDate(mGradeBean)
                .setOnClickListener(v -> {
                    if (Latte.getActivity() != null) {
                        Latte.getActivity().finish();
                    }
                })
                .show();
    }


}
