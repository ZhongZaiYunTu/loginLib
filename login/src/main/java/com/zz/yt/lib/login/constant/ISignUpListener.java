package com.zz.yt.lib.login.constant;

import android.view.View;
import android.widget.TextView;


/**
 * 注册
 *
 * @author qf
 * @date 2020-7-12
 */
public interface ISignUpListener {

    /**
     * 是否显示注册
     *
     * @return true
     */
    boolean isSignUp();


    /**
     * 注册按钮设置
     *
     * @param rootView:父view
     * @param signUpView:注册按钮
     */
    void setSignUpView(View rootView, TextView signUpView);

}
