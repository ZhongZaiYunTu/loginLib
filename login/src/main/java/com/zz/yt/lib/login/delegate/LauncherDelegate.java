package com.zz.yt.lib.login.delegate;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.blankj.utilcode.util.ThreadUtils;
import com.whf.android.jar.app.Latte;
import com.whf.android.jar.util.log.LatteLogger;
import com.zz.yt.lib.login.R;
import com.zz.yt.lib.login.checker.AccountManager;
import com.zz.yt.lib.login.checker.IUserChecker;
import com.zz.yt.lib.login.delegate.login.base.BaseSigInDelegate;
import com.zz.yt.lib.login.launcher.ILauncherListener;
import com.zz.yt.lib.login.launcher.OnLauncherFinishTag;
import com.zz.yt.lib.login.launcher.privacy.ScrollLauncher;
import com.zz.yt.lib.login.launcher.privacy.PrivacyLauncher;

import java.util.concurrent.TimeUnit;



/**
 * 欢迎页
 *
 * @author qf
 * @version 2020-03-12
 */
public class LauncherDelegate extends BaseSigInDelegate {

    private int suspend = 2;
    private TextView idText;
    private ILauncherListener mLauncherListener = null;
    /**
     * 是否点击跳转
     */
    private boolean isJump = false;

    @NonNull
    public static LauncherDelegate create() {
        Bundle args = new Bundle();

        final LauncherDelegate fragment = new LauncherDelegate();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onAttach(@NonNull Context activity) {
        super.onAttach(activity);
        if (activity instanceof ILauncherListener) {
            mLauncherListener = (ILauncherListener) activity;
        }
    }

    @Override
    public Object setLayout() {
        return R.layout.hai_delegate_launcher;
    }

    private void inBundle() {
        if (mLauncherListener != null) {
            Object launcher = mLauncherListener.setLauncher();
            if (launcher != null) {
                if (launcher instanceof Integer) {
                    getImageView(R.id.login_image_background)
                            .setImageResource((int) launcher);
                } else if (launcher instanceof View) {
                    View viewLauncher = (View) launcher;
                    RelativeLayout view = findViewById(R.id.login_layout_background);
                    ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.MATCH_PARENT);
                    viewLauncher.setLayoutParams(layoutParams);
                    view.addView(viewLauncher);
                }
            }
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindView(@Nullable Bundle savedInstanceState, @NonNull View rootView) {
        inBundle();
        suspend = Latte.getAppGuide();
        idText = getTextView(R.id.login_text_timer);
        idText.setText("跳过(" + suspend + ")");
        idText.setVisibility(Latte.isAppGuide() ? View.VISIBLE : View.GONE);
        idText.setOnClickListener(view -> {
            isJump = true;
            checkIsShowScroll();
        });
        startTiming();
        //检查用户是否登录了APP
        AccountManager.checkAccount(new IUserChecker() {
            @Override
            public void onSignIn() {
                //登录时；增加更新检查。
                inGradePermission();
            }

            @Override
            public void onNotSignIn() {
                //还未登录，进登录页
            }
        });
    }

    /**
     * 倒计时
     */
    private void startTiming() {
        ThreadUtils.executeByFixedAtFixRate(1, new ThreadUtils.SimpleTask<String>() {

            @Override
            public String doInBackground() {
                return suspend > 0 ? "跳过(" + suspend + ")" : "跳过";
            }

            @Override
            public void onSuccess(@Nullable String result) {
                if (idText != null) {
                    idText.setText(result);
                }
                suspend--;
                if (isJump) {
                    cancel();
                } else {
                    if (suspend < 1) {
                        cancel();
                        checkIsShowScroll();
                    }
                }
            }
        }, 1, TimeUnit.SECONDS);
    }

    /**
     * 判断是否显示滑动启动页
     */
    private void checkIsShowScroll() {
        if (PrivacyLauncher.isPrivacyLauncher()) {
            //判断是否显示.用户隐私保护；
            getSupportDelegate().start(LauncherPrivacyDelegate.create(), SINGLETASK);
            return;
        }
        if (ScrollLauncher.isScrollLauncher()) {
            getSupportDelegate().start(LauncherScrollDelegate.create(), SINGLETASK);
        } else {
            //检查用户是否登录了APP
            AccountManager.checkAccount(new IUserChecker() {
                @Override
                public void onSignIn() {
                    if (isGranted()) {
                        LatteLogger.i("需要更新，不直接跳转到首页");
                        return;
                    }
                    if (mLauncherListener != null) {
                        mLauncherListener.onLauncherFinish(OnLauncherFinishTag.SIGNED);
                    }
                }

                @Override
                public void onNotSignIn() {
                    if (mLauncherListener != null) {
                        mLauncherListener.onLauncherFinish(OnLauncherFinishTag.NOT_SIGNED);
                    }
                }
            });
        }
    }

    @Override
    protected void onGrantedSuccess() {

    }
}
