package com.zz.yt.lib.login.delegate.login;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.flyco.tablayout.CommonTabLayout;
import com.flyco.tablayout.listener.CustomTabEntity;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.whf.android.jar.app.Latte;
import com.zz.yt.lib.login.R;
import com.zz.yt.lib.login.checker.AccountManager;
import com.zz.yt.lib.login.checker.AgreementPrivacyManager;
import com.zz.yt.lib.login.constant.ILoginListener;
import com.zz.yt.lib.login.constant.ISignCodeListener;
import com.zz.yt.lib.login.constant.ISignUpListener;
import com.zz.yt.lib.login.delegate.login.base.BaseFingerDelegate;
import com.zz.yt.lib.login.view.AccountPassView;
import com.zz.yt.lib.ui.listener.OnClickBoolListener;
import com.zz.yt.lib.ui.recycler.data.TabEntity;
import com.zz.yt.lib.ui.view.phone.PhoneCodeView;

import java.util.ArrayList;


/**
 * 账号与手机号切换登录
 *
 * @author qf
 * @version 20.1.3
 */
public class LoginTabDelegate extends BaseFingerDelegate implements OnClickBoolListener {


    private AccountPassView accountPassView;
    private PhoneCodeView phoneCodeView;
    private Button loginSign;

    private ILoginListener mLoginListener = null;
    private ISignCodeListener mSignCodeListener = null;
    private ISignUpListener mSignUpListener = null;

    @Override
    public void onAttach(@NonNull Context activity) {
        super.onAttach(activity);
        //这里是类型转换，而不是判断
        if (activity instanceof ILoginListener) {
            mLoginListener = (ILoginListener) activity;
        }
        if (activity instanceof ISignCodeListener) {
            mSignCodeListener = (ISignCodeListener) activity;
        }
        if (activity instanceof ISignUpListener) {
            mSignUpListener = (ISignUpListener) activity;
        }
    }

    @Override
    public Object setLayout() {
        return R.layout.hai_delegate_login_tab;
    }

    private void inBundle() {
        if (mLoginListener != null) {
            Object launcher = mLoginListener.setSignIn();
            if (launcher != null) {
                if (launcher instanceof Integer) {
                    getImageView(R.id.login_image_background)
                            .setImageResource((int) launcher);
                } else if (launcher instanceof View) {
                    View viewLauncher = (View) launcher;
                    RelativeLayout view = findViewById(R.id.login_layout_background);
                    ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.MATCH_PARENT);
                    viewLauncher.setLayoutParams(layoutParams);
                    view.addView(viewLauncher);
                }
            }
        }

        //登录状态为未登录
        AccountManager.setSignState(false);

        //注册
        boolean isSignUp = false;
        if (mSignUpListener != null) {
            isSignUp = mSignUpListener.isSignUp();
            mSignUpListener.setSignUpView(mRootView, getTextView(R.id.login_text_sign_up));
        }
        getTextView(R.id.login_text_sign_up).setVisibility(isSignUp ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onBindView(@Nullable Bundle savedInstanceState, @NonNull View rootView) {
        inBundle();
        initView();
        inGradePermission();
    }

    private void initView() {
        accountPassView = findViewById(R.id.id_account_pass_view);
        phoneCodeView = findViewById(R.id.id_phone_code_view);
        if (accountPassView != null) {
            accountPassView.setOnClickBoolListener(this);
        }
        if (phoneCodeView != null) {
            phoneCodeView.setOnClickBoolListener(this);
        }

        loginSign = getButton(R.id.login_btn_sign_in);

        if (mLoginListener != null) {
            mLoginListener.setSignInView(loginSign);
            //测试服务器
            TextView textDebug = getTextView(R.id.text_debug);
            if (textDebug != null) {
                mLoginListener.setServiceSport(textDebug);
            }
            findViewById(R.id.layout_text_debug).setVisibility(Latte.isSwitchingServices()
                    ? View.VISIBLE : View.GONE);
        }


        CommonTabLayout commonTabLayout = findViewById(R.id.vi_tab_login);
        if (commonTabLayout != null) {
            ArrayList<CustomTabEntity> tabEntities = new ArrayList<>();
            tabEntities.add(new TabEntity("账号密码登录"));
            tabEntities.add(new TabEntity("短信验证码登录"));
            commonTabLayout.setTabData(tabEntities);
            commonTabLayout.setOnTabSelectListener(new OnTabSelectListener() {
                @Override
                public void onTabSelect(int position) {
                    if (accountPassView != null) {
                        accountPassView.setShow(position == 0);
                    }
                    if (phoneCodeView != null) {
                        phoneCodeView.setShow(position == 1);
                    }
                }

                @Override
                public void onTabReselect(int position) {

                }
            });
        }
    }


    @Override
    protected void onGrantedSuccess() {
        if (phoneCodeView != null) {
            phoneCodeView.setOnClickStringListener(phone -> {
                if (mSignCodeListener != null) {
                    mSignCodeListener.onPhoneCode(phone);
                }
            });
        }
        loginSign.setOnClickListener(view -> {
            if (isGranted()) {
                setInstallPermission();
                return;
            }
            if (AgreementPrivacyManager.is()) {
                return;
            }
            if (mLoginListener != null && accountPassView != null && accountPassView.isShow()) {
                String account = accountPassView.getAccount();
                String password = accountPassView.getPassword();
                if (isAccount(account) && isPassword(password)) {
                    mLoginListener.onSignIn(account, password);
                }
            }

            if (mSignCodeListener != null && phoneCodeView != null && phoneCodeView.isShow()) {
                String phone = phoneCodeView.getPhone();
                String phoneCode = phoneCodeView.getPhoneCode();
                if (isPhone(phone) && isCode(phoneCode)) {
                    phoneCodeView.setTiming(true);
                    mSignCodeListener.onPhoneSignIn(phone, phoneCode);
                }
            }
        });
    }


    @Override
    public void onClick(boolean enabled) {
        if (loginSign != null) {
            loginSign.setEnabled(enabled);
        }
    }
}