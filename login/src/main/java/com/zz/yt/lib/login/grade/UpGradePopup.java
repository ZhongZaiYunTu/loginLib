package com.zz.yt.lib.login.grade;

import android.content.Context;
import android.view.View;

import androidx.annotation.NonNull;

import com.tencent.smtt.sdk.WebView;
import com.whf.android.jar.popup.LattePopupCenter;
import com.zz.yt.lib.login.R;
import com.zz.yt.lib.login.grade.bean.GradeBean;

/**
 * 升级
 *
 * @author qf
 * @version 22.3.12
 */
class UpGradePopup extends LattePopupCenter implements View.OnClickListener {

    private GradeBean mGradeBean = null;

    @NonNull
    static UpGradePopup create(Context context) {
        return new UpGradePopup(context);
    }

    private UpGradePopup(Context context) {
        super(context);
    }

    @Override
    protected int setLayout() {
        return R.layout.hai_popup_up_grade;
    }

    UpGradePopup setGradeBean(GradeBean gradeBean) {
        this.mGradeBean = gradeBean;
        return this;
    }

    @Override
    protected void initViews() {
        setOnClickListener(R.id.login_confirm, this);
        final WebView webView = findViewById(R.id.login_web_scroll);
        webView.loadDataWithBaseURL("", mGradeBean.getContent(), "text/html", "utf-8", null);
    }

    @Override
    public void onClick(View v) {
        if (mClickDecisionListener != null) {
            mClickDecisionListener.onConfirm(null);
        }
        dismiss();
    }

}
