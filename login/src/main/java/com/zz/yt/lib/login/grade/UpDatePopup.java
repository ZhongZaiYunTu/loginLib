package com.zz.yt.lib.login.grade;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.View;
import android.widget.Button;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;

import com.liys.view.LineProView;
import com.tencent.smtt.sdk.WebView;
import com.whf.android.jar.popup.LattePopupCenter;
import com.zz.yt.lib.login.R;
import com.zz.yt.lib.login.grade.bean.GradeBean;


/**
 * 升级
 *
 * @author qf
 * @version 22.3.12
 */
class UpDatePopup extends LattePopupCenter implements View.OnClickListener {

    private final static int MAX = 100;

    private GradeBean mGradeBean = null;

    @NonNull
    static UpDatePopup create(Context context) {
        return new UpDatePopup(context);
    }

    private UpDatePopup(Context context) {
        super(context);
    }

    @Override
    protected int setLayout() {
       return R.layout.hai_popup_up_date;
    }

    UpDatePopup setGradeBean(GradeBean gradeBean) {
        this.mGradeBean = gradeBean;
        return this;
    }
    @Override
    protected void initViews() {
        final Button mOkBtn = findViewById(R.id.login_confirm);
        final Button mCancelBtn = findViewById(R.id.login_cancel);
        setText(R.id.login_text_versions, mGradeBean.getVersionName());
        mOkBtn.setOnClickListener(this);
        mCancelBtn.setOnClickListener(v -> {
            if (mClickDecisionListener != null) {
                mClickDecisionListener.onCancel();
            }
            dismiss();
        });
        final WebView webView = findViewById(R.id.login_web_scroll);
        webView.loadDataWithBaseURL("", mGradeBean.getContent(), "text/html", "utf-8", null);
    }

    @Override
    public void onClick(View v) {
        if (mClickDecisionListener != null) {
            mClickDecisionListener.onConfirm(null);
        }
        setVisibility(R.id.login_layout_progress, true);
        setVisibility(R.id.login_layout_button, false);
    }


    private void setVisibility(@IdRes int viewId, boolean isText) {
        findViewById(viewId).setVisibility(isText ? View.VISIBLE : View.GONE);
    }

    @SuppressLint("SetTextI18n")
    void setProgressBar(int progress) {
        final LineProView proView = findViewById(R.id.id_line_pro_view);
        if (proView != null && progress > 0 && progress <= MAX) {
            proView.setProgress(progress);
            if (progress == MAX) {
                dismiss();
            }
        }
    }

}
