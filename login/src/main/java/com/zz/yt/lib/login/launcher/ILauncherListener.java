package com.zz.yt.lib.login.launcher;

import androidx.annotation.NonNull;

import java.util.ArrayList;

/**
 * 滑动启动页
 *
 * @author 傅令杰
 * @version 2017/4/22
 */

public interface ILauncherListener {

    /**
     * 用户是否已经登录
     *
     * @param tag:
     */
    void onLauncherFinish(@NonNull OnLauncherFinishTag tag);


    /**
     * 设置启动背景图片
     *
     * @return 图片
     */
    Object setLauncher();

    /**
     * 设置引导图片
     *
     * @return 图片 int或者String
     */
    ArrayList<Object> setLauncherScroll();
}
