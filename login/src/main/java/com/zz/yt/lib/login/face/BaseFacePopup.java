package com.zz.yt.lib.login.face;

import android.content.Context;
import android.view.View;

import com.blankj.utilcode.util.KeyboardUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.CenterPopupView;
import com.whf.android.jar.app.Latte;
import com.whf.android.jar.popup.LattePopupCenter;
import com.zz.yt.lib.login.R;
import com.zz.yt.lib.login.face.eventbus.FaceKeys;
import com.zz.yt.lib.login.face.listener.OnClickFaceListener;


import org.jetbrains.annotations.NotNull;

import io.fotoapparat.Fotoapparat;
import io.fotoapparat.parameter.ScaleType;
import io.fotoapparat.preview.Frame;
import io.fotoapparat.preview.FrameProcessor;
import io.fotoapparat.selector.FocusModeSelectorsKt;
import io.fotoapparat.selector.LensPositionSelectorsKt;
import io.fotoapparat.selector.ResolutionSelectorsKt;
import io.fotoapparat.selector.SelectorsKt;
import io.fotoapparat.view.CameraView;

import static com.blankj.utilcode.util.ViewUtils.runOnUiThread;

/**
 * @author qf
 * @author 中再云图技术有限公司
 * @version 1.0.26
 **/
public abstract class BaseFacePopup extends CenterPopupView implements FrameProcessor {

    final Context mContext;

    OnClickFaceListener mClickFaceListener = null;

    private boolean isCloseTime = true;
    private long closeTime = 0;
    private long oldTime = 0;

    /**
     * 相机
     */
    private Fotoapparat fotoapparat;

    /**
     * 是否正在检测人脸
     */
    boolean isDetectingFace = false;

    protected BaseFacePopup(Context context) {
        super(context);
        this.mContext = context;
    }

    public BaseFacePopup setOnClickListener(OnClickFaceListener listener) {
        this.mClickFaceListener = listener;
        return this;
    }

    //************************************* 重写界面 *****************************************/

    /*** 布局文件 */
    protected abstract int setLayout();

    /*** 初始化 */
    protected abstract void initViews();

    @Override
    protected int getImplLayoutId() {
        return setLayout();
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        initViews();
    }

    void initCameraView() {
        //初始化摄像头
        final CameraView mCameraView = findViewById(R.id.login_camera_view);
        fotoapparat = Fotoapparat
                .with(mContext)
                .into(mCameraView)
                .previewScaleType(ScaleType.CenterCrop)
                .photoResolution(ResolutionSelectorsKt.highestResolution())
                .lensPosition(LensPositionSelectorsKt.front())
                .focusMode(SelectorsKt.firstAvailable(
                        FocusModeSelectorsKt.continuousFocusPicture(),
                        FocusModeSelectorsKt.autoFocus(),
                        FocusModeSelectorsKt.fixed()
                ))
                .frameProcessor(this)
                .build();
    }

    /**
     * 显示在中间
     */
    public final void start() {
        KeyboardUtils.hideSoftInput(Latte.getActivity());
        new XPopup.Builder(Latte.getActivity())
                .moveUpToKeyboard(false)
                .enableDrag(true)
                .isDestroyOnDismiss(false)
                .isThreeDrag(false)
                .keepScreenOn(true)
                .dismissOnTouchOutside(false)
                .asCustom(this)
                .show();
        if (fotoapparat != null) {
            fotoapparat.start();
        }
    }

    @Override
    public void dismiss() {
        super.dismiss();
        close();
        isCloseTime = true;
    }

    /**
     * 成功关闭
     */
    void close() {
        if (fotoapparat != null) {
            fotoapparat.stop();
        }
        runOnUiThread(this::dismiss);
    }

    /**
     * 超时关闭
     */
    void overtime() {
        long newTime = System.currentTimeMillis();
        if (isCloseTime) {
            closeTime = System.currentTimeMillis();
        }
        isCloseTime = false;
        long times = newTime - closeTime;
        //保证性能，0.2秒识别一次
        if (times < FaceKeys.OVERTIME) {
            return;
        }
        ToastUtils.showShort("识别失败，请稍后再试");
        close();
        isCloseTime = true;
    }

    @Override
    public void process(@NotNull Frame frame) {
        long newTime = System.currentTimeMillis();
        long times = newTime - oldTime;
        //保证性能，0.2秒识别一次
        if (times < FaceKeys.FACE) {
            return;
        }
        oldTime = System.currentTimeMillis();
    }


}
