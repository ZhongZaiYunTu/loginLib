package com.zz.yt.lib.login.popup;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.hardware.fingerprint.FingerprintManager;
import android.os.CancellationSignal;

import android.widget.TextView;

import com.blankj.utilcode.util.ToastUtils;
import com.whf.android.jar.app.Latte;
import com.zz.yt.lib.login.R;
import com.zz.yt.lib.ui.popup.view.base.BaseCenterPopup;

import org.jetbrains.annotations.NotNull;

import javax.crypto.Cipher;

/**
 * 提示指纹登录
 *
 * @author qf
 * @version 22.4.22
 */
@TargetApi(23)
@SuppressLint("ViewConstructor")
public final class FingerprintPopup extends BaseCenterPopup {

    final Cipher mCipher;

    private FingerprintManager fingerprintManager;
    private CancellationSignal mCancellationSignal;

    private TextView errorMsg;

    /**
     * 标识是否是用户主动取消的认证。
     */
    private boolean isSelfCancelled;


    @NotNull
    public static FingerprintPopup create(Cipher cipher) {
        return new FingerprintPopup(cipher);
    }

    private FingerprintPopup(Cipher cipher) {
        super(Latte.getActivity());
        this.mCipher = cipher;
    }
    @Override
    protected int setLayout() {
        return R.layout.hai_popup_fingerprint;
    }

    @Override
    protected void initViews() {
        fingerprintManager = Latte.getActivity().getSystemService(FingerprintManager.class);
        errorMsg = findViewById(R.id.login_error_msg);
        TextView cancel = findViewById(R.id.login_cancel);
        cancel.setOnClickListener(v -> {
            dismiss();
            stopListening();
            if (mClickDecisionListener != null) {
                mClickDecisionListener.onCancel();
            }
        });
        // 开始指纹认证监听
        startListening(mCipher);
    }

    @Override
    public void onDismiss() {
        super.onDismiss();
        // 停止指纹认证监听
        stopListening();
    }

    private void startListening(Cipher cipher) {
        isSelfCancelled = false;
        mCancellationSignal = new CancellationSignal();
        fingerprintManager.authenticate(new FingerprintManager.CryptoObject(cipher), mCancellationSignal, 0,
                new FingerprintManager.AuthenticationCallback() {
                    @Override
                    public void onAuthenticationError(int errorCode, CharSequence errString) {
                        if (!isSelfCancelled) {
                            errorMsg.setText(errString);
                            if (errorCode == FingerprintManager.FINGERPRINT_ERROR_LOCKOUT) {
                                ToastUtils.showLong(errString);
                                dismiss();
                            }
                        }
                    }

                    @Override
                    public void onAuthenticationHelp(int helpCode, CharSequence helpString) {
                        errorMsg.setText(helpString);
                    }

                    @Override
                    public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {
                        dismiss();
                        ToastUtils.showLong("指纹认证成功");
                        if (mClickDecisionListener != null) {
                            mClickDecisionListener.onConfirm("指纹认证成功");
                        }
                    }

                    @Override
                    public void onAuthenticationFailed() {
                        errorMsg.setText("指纹认证失败，请再试一次");
                    }
                }, null);
    }

    private void stopListening() {
        if (mCancellationSignal != null) {
            mCancellationSignal.cancel();
            mCancellationSignal = null;
            isSelfCancelled = true;
        }
    }


}
