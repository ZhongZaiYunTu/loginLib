# SignIn 版本
[![](https://jitpack.io/v/com.gitee.ZhongZaiYunTu/loginLib.svg)](https://jitpack.io/#com.gitee.ZhongZaiYunTu/loginLib)


#### 介绍
登录组件


#### 安装教程

1.加上网址


        allprojects {

		repositories {
			...
			maven { url 'https://jitpack.io' }
		}
	}

2.使用

        dependencies {

	        implementation 'com.gitee.ZhongZaiYunTu:loginLib:Tag'
	}


#### 使用说明

1.使用启动页和登录页

        package com.zz.yt.test;
        
        import com.blankj.utilcode.util.ActivityUtils;
        import com.blankj.utilcode.util.ToastUtils;
        import com.zz.yt.lib.login.base.BaseProxyActivity;
        import com.zz.yt.lib.login.base.LatteDelegate;
        import com.zz.yt.lib.login.constant.ISignCodeListener;
        import com.zz.yt.lib.login.delegate.LauncherDelegate;
        import com.zz.yt.lib.login.delegate.login.SignInCodeDelegate;
        import com.zz.yt.lib.login.launcher.ILauncherListener;
        import com.zz.yt.lib.login.launcher.OnLauncherFinishTag;
        
        import java.util.ArrayList;
        
        /**
         * @author Administrator
         */
        public class LoginActivity extends BaseProxyActivity implements ILauncherListener, ISignCodeListener {
        
        
            @Override
            public LatteDelegate setRootDelegate() {
                return new LauncherDelegate();
            }
        
            @Override
            public void post(Runnable runnable) {
        
            }
        
            @Override
            public void onLauncherFinish(OnLauncherFinishTag tag) {
                switch (tag) {
                    case SIGNED:
                        ActivityUtils.startActivity(MainActivity.class);
                        break;
                    case NOT_SIGNED:
                        getSupportDelegate().startWithPop(new SignInCodeDelegate());
                        break;
                    default:
                        break;
                }
            }
        
            @Override
            public Object setLauncher() {
                return R.mipmap.launcher_01;
            }
        
            @Override
            public ArrayList<Integer> setLauncherScroll() {
                ArrayList<Integer> arrayList = new ArrayList<>();
                arrayList.add(R.mipmap.launcher_01);
                arrayList.add(R.mipmap.launcher_02);
                arrayList.add(R.mipmap.launcher_03);
                arrayList.add(R.mipmap.launcher_04);
                arrayList.add(R.mipmap.launcher_05);
                return arrayList;
            }
        
            @Override
            public Object setSignIn() {
                return null;
            }
        
            @Override
            public void onSignIn(String phone, String code) {
                ActivityUtils.startActivity(MainActivity.class);
            }
        
            @Override
            public void onPhoneCode(String phone) {
        
            }
        
            @Override
            public void upGrade() {
        
            }
        
            @Override
            public void onUserAgreement() {
                ToastUtils.showShort("用户协议");
            }
        
            @Override
            public void onPrivacyClause() {
                ToastUtils.showShort("隐私条款");
            }
        
        }


#### 参与贡献

